/**
 * Copyright (c) 2011-2020, hubin (jobob@qq.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.alinesno.cloud.compoment.generate.generator.config;

import java.io.File;
import java.nio.charset.Charset;

/**
 * 定义常量
 * @author LuoAnDong
 * @since 2018年2月14日 下午2:01:32
 */
public interface ConstVal {

    public static final String MODULENAME = "ModuleName";

    // 名称后缀
    public static final String ENTITY = "Entity";
    public static final String SERIVCE = "Service";
    public static final String SERVICEIMPL = "ServiceImpl";
    public static final String SERVICE_AUTO_IMPL = "ServiceAutoImpl";
    public static final String MAPPER = "Mapper";
    public static final String XML = "Xml";
    public static final String CONTROLLER = "Controller";
	public static final String PAGE = "Page";
	public static final String REPOSITORY = "Repository";
	public static final String REST_CONTROLLER= "RestController";
	public static final String FEIGIN = "Feigin";
	public static final String FEIGIN_DTO = "Dto";
	public static final String BOOT = "Boot";

    public static final String ENTITY_PATH = "entity_path";
    public static final String SERIVCE_PATH = "serivce_path";
    public static final String SERVICEIMPL_PATH = "serviceimpl_path";
    public static final String MAPPER_PATH = "mapper_path";
    public static final String XML_PATH = "xml_path";
    public static final String CONTROLLER_PATH = "controller_path";
	public static final String REPOSITORY_PATH = "repository_path" ;
    public static final String REST_CONTROLLER_PATH = "rest_path";
   
    // feigin 支持
    public static final String FEIGIN_API_PATH = "feigin_facade_path";
    public static final String FEIGIN_DTO_PATH = "feigin_dto_path";
    
    //jsp 页面
    public static final String PAGE_LIST_PATH = "page_list_path" ;
	public static final String PAGE_ADD_PATH = "page_add_path" ;
	public static final String PAGE_MODIFY_PATH = "page_modify_path" ;
	
	// springboot 启动类
	public static final String BOOT_PATH = "boot_path" ;

    public static final String JAVA_TMPDIR = "java.io.tmpdir";
    public static final String UTF8 = Charset.forName("UTF-8").name();
    public static final String UNDERLINE = "_";

    public static final String JAVA_SUFFIX = ".java";
    public static final String XML_SUFFIX = ".xml";
    public static final String JSP_SUFFIX = ".jsp" ; 

    public static final String TEMPLATE_ENTITY = "entity.java.vm";
    public static final String TEMPLATE_MAPPER = "mapper.java.vm";
    public static final String TEMPLATE_XML = "mapper.xml.vm";
    public static final String TEMPLATE_SERVICE = "service.java.vm";
    public static final String TEMPLATE_SERVICEIMPL = "serviceImpl.java.vm";
    public static final String TEMPLATE_CONTROLLER = "controller.java.vm";
    
	public static final String TEMPLATE_REPOSITORY = "repository.java.vm"; //定义持久层模板
	public static final String TEMPLATE_REST_CONTROLLER = "rest.controller.java.vm"; // 对外提供Rest接口控制层
	public static final String TEMPLATE_FEIGN_API = "feign.facade.java.vm"; // 对外提供Feign接口
	public static final String TEMPLATE_FEIGN_DTO = "feign.dto.java.vm"; // 对外提供Feign接口传输对象
	public static final String TEMPLATE_BOOT = "boot.java.vm"; //针对于springboot启动类


    public static final String ENTITY_NAME = File.separator + "%s" + JAVA_SUFFIX;

    // 配置使用classloader加载资源
    public static final String VM_LOADPATH_KEY = "file.resource.loader.class";
    public static final String VM_LOADPATH_VALUE = "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader";

    public static final String SUPERD_MAPPER_CLASS = "com.baomidou.mybatisplus.mapper.BaseMapper";
    public static final String SUPERD_SERVICE_CLASS = "com.baomidou.mybatisplus.service.IService";
    public static final String SUPERD_SERVICEIMPL_CLASS = "com.baomidou.mybatisplus.service.impl.ServiceImpl";
    
	// rest controller 父类
	public static final String SUPER_REST_CONTROLLER_CLASS = "com.alinesno.cloud.common.core.rest.BaseRestController" ;
	
	// feigin dto 父类
	public static final String SUPER_FEIGIN_DTO_CLASS = "com.alinesno.cloud.common.facade.base.BaseDto" ;

    // jsp管理页面
	public static final String TEMPLATE_JSP_LIST_PATH = "page.list.jsp.vm";
	public static final String TEMPLATE_JSP_ADD_PATH = "page.add.jsp.vm";
	public static final String TEMPLATE_JSP_MODIFY_PATH = "page.modify.jsp.vm";

	// spring cloud datajpa类型
	public static final String SUPER_REPOSITORY_CLASS = "com.alinesno.cloud.common.core.orm.repository.IBaseJpaRepository";
	public static final String ID_KEY_TYPE = "java.lang.String";



}
