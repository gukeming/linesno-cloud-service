<h2 style="text-align:center"> 服务化开发平台</h2>

> 此基线设计更适合基础架构研发组,为开发组提供研发组件

alinesno-cloud是国内首个基于`Spring Cloud`的`服务开发平台`，产出方向为企业基础架构和统一研发云平台，
整体平台从
<br/>
<span style="font-weight:bold">`基础规范` - `组织结构` - `基础架构` - `业务开发` - `持续集成`- `自动化部署` - `自动化测试` - ` 生产运维监控` - `在线升级`</span>
<br/>
的全方位企业级云平台开发解决方案，具有统一授权、认证后台管理系统，其中包含具备用户管理、资源权限管理、网关API
管理等多个模块，结合多个组件，为开发提供基础开发架构和支持，同时结合多种单点登陆方式(Cookies+SpringSession和Cas)，支持多业务系统并行开发。代码简洁，架构清晰，适合学习和直接项目(后期支持)中使用。
核心技术采用`Spring Boot 2.1.4`以及`Spring Cloud (Greenwich.RELEASE)`相关核心组件。
<br/>
<br/>
<p align="center">
    <img src="https://img.shields.io/badge/Avue-1.5.0-green.svg" alt="Build Status">
    <img src="https://img.shields.io/badge/Spring%20Cloud-Greenwich.RELEASE-blue.svg" alt="Coverage Status">
    <img src="https://img.shields.io/badge/Spring%20Boot-2.1.4.RELEASE-blue.svg" alt="Downloads">
</p>

#### 相关基线
| 序号 | 基线说明                     | 基线地址                           | 在线文档                 | 状态   | 备注 |
|------|------------------------------|------------------------------------|--------------------------|--------|------|
| 1    | 研发平台整体规划方案         | [linesno-cloud-document][document] | [在线文档][document]     | 集成中 |      |
| 1    | 平台环境搭建文档记录文档基线 | [linesno-cloud-env][env]           | [在线文档][env_link]     | 集成中 |      |
| 2    | 研发人员服务列表代码基线     | [linesno-cloud-service][service]   | [在线文档][service_link] | 集成中 |      |
| 3    | 开发人员使用平台指引教程     | [linesno-cloud-guide][guide]       | [在线文档][guide_link]   | 集成中 | .    |

[document]: http://gitbook.linesno.com/linesno-cloud-document/_book/
[env]: https://gitee.com/landonniao/linesno-cloud-env
[env_link]: http://gitbook.linesno.com/linesno-cloud-env/_book/
[service]: https://gitee.com/landonniao/linesno-cloud-service
[service_link]: http://gitbook.linesno.com/linesno-cloud-service/_book/
[guide]: https://gitee.com/landonniao/linesno-cloud-guide
[guide_link]: http://gitbook.linesno.com/linesno-cloud-guide/_book/

#### 架构设计
此处设计并没有按网络或者相关论坛生搬硬照,会去掉一些操作如持续集成添加代码检测、使用cloud全家桶，
这些都可能会导致开发过程或者后期隐患，建议按企业实际团队情况考虑。

##### 业务架构设计
> 图片为练习项目的开发架构，与项目略有不一致，思路一致

<p style="text-align:center"><img src="/images/design_00.png" width="100%" /></p>

##### 业务架构设计
> 图片为练习项目的开发架构，与项目略有不一致，思路一致

<p style="text-align:center"><img src="/images/design_04.png" width="80%" /></p>

##### 技术架构设计
> 图片为练习项目的技术架构，与项目略有不一致，思路一致

<p style="text-align:center"><img src="/images/design_03.png" width="80%" /></p>

##### 部署架构设计
> 图片为练习项目的技术架构，与项目略有不一致，思路一致

<p style="text-align:center"><img src="/images/design_05.png" width="80%" /></p>

##### 持续集成设计
> 图片为练习项目的技术架构，与项目略有不一致，思路一致

<p style="text-align:center"><img src="/images/design_06.png" width="80%" /></p>

##### 功能集成进度

| 序号 | 功能名称         | 开发人员 | 集成情况 | 备注 |
|------|------------------|----------|----------|------|
| 1    | 默认功能         | Switch   | 完成     |      |
| 2    | 代码生成器说明   | Switch   | 集成中   |      |
| 3    | 单点接入         |          |          |      |
| 4    | 服务集成教程     |          |          |      |
| 5    | 组件集成教程     |          |          |      |
| 6    | 前端集成教程     |          |          |      |
| 7    | 发布教程         |          |          |      |
| 8    | 服务调用         |          |          |      |
| 9    | 异常处理         |          |          |      |
| 10   | 日志处理         |          |          |      |
| 11   | 缓存使用         |          |          |      |
| 12   | 消息使用         |          |          |      |
| 13   | 分布式事务       |          |          |      |
| 14   | 分布式锁         |          |          |      |
| 15   | 分布式限流       |          |          |      |
| 16   | 重复提交         |          | 完成     |      |
| 17   | 权限管理         |          |          |      |
| 18   | 工作流使用       |          |          |      |
| 19   | 短信和邮件使用   | Switch   | 集成中   |      |
| 20   | 存储使用         |          | 集成中   |      |
| 21   | 云桌面使用       |          |          |      |
| 22   | 接口组件使用     |          |          |      |
| 23   | 支付接口使用     |          |          |      |
| 24   | 日志服务使用     |          |          |      |
| 25   | 打印签名服务使用 |          |          |      |
| 26   | 分布式配置使用   |          |          |      |
| 27   | 日志埋点         |          |          |      |
| 28   | 验证码开启       |          |          |      |
| 29   | 定时任务         |          |          |      |
| 30   | 多数据库源配置   |          |          |      |


#### 功能截图

<p style="text-align:center"><img src="/images/demo_01.jpg" width="80%" /></p>

<p style="text-align:center"><img src="/images/demo_02.jpg" width="80%" /></p>

<p style="text-align:center"><img src="/images/demo_03.jpg" width="80%" /></p>

<p style="text-align:center"><img src="/images/demo_04.jpg" width="80%" /></p>

<p style="text-align:center"><img src="/images/demo_05.jpg" width="80%" /></p>

<p style="text-align:center"><img src="/images/demo_06.jpg" width="80%" /></p>

#### 设计原则
- 服务单库设计,以减少迁移，服务之前影响等；
- 基础服务只为调用设计，位于服务的底层或者中间层，基础服务禁止调用业务服务；
- 业务服务调用基础服务，或者其它服务，业务服务为服务的顶层，用于定制化业务；
- 同一级服务之间可以互相调用，只能自下往下调用，平级调用，禁止自下往上调用，以避免服务混乱及维护混乱。
- 每种服务目录按999个服务规划

### 公共工程
| 序号 | 服务名称 | 服务别名                         | 端口 | 集成情况 | 备注               |
|------|----------|----------------------------------|------|----------|--------------------|
| 1    | 父类工程 | alinesno-cloud-commom-parent     |      | 完成     |                    |
| 2    | 配置工程 | alinesno-cloud-common-config     |      | 集成中   |                    |
| 3    | 核心工程 | alinesno-cloud-common-core       |      | 集成中   |                    |
| 4    | 工具工程 | ~~alinesno-cloud-common-utils~~  |      | 完成     | 换成hutool         |
| 5    | 静态工程 | ~~alinesno-cloud-common-static~~ |      | 完成     | 放置在common-web中 |
| 6    | 页面工程 | ~~alinesno-cloud-common-pages~~  |      |          | 删除               |
| 7    | 前端工程 | alinesno-cloud-common-web        |      | 完成     | 略                 |

#### 目录规划

##### 组件规划
| 序号 | 服务名称     | 服务别名                             | 端口 | 集成情况 | 备注 |
|------|--------------|--------------------------------------|------|----------|------|
| 1    | 代码生成组件 | alinesno-cloud-component-generate    |      | 完成     |      |
| 1    | 存储组件     | alinesno-cloud-compoment-storage     |      | 集成中   |      |
| 2    | 地图组件     | alinesno-cloud-compoment-map         |      |          |      |
| 3    | 缓存组件     | alinesno-cloud-compoment-chche       |      |          |      |
| 4    | 消息组件     | ~~alinesno-cloud-compoment-message~~ |      |          | 删除 |
| 5    | 接口组件     | alinesno-cloud-compoment-socket      |      |          |      |
| 5    | 通知组件     | ~~alinesno-cloud-compoment-notice~~  |      |          | 删除 |
| 6    | 邮件组件     | ~~alinesno-cloud-compoment-email~~   |      |          | 删除 |
| 7    | 短信组件     | ~~alinesno-cloud-compoment-sms~~     |      |          | 删除 |

##### 监控工程
| 序号 | 服务名称 | 服务别名                     | 端口   | 集成情况 | 备注 |
|------|----------|------------------------------|--------|----------|------|
| 1    | 收集组件 | alinesno-cloud-monitor-agent |        |          |      |
| 2    | 监控应用 | alinesno-cloud-monitor-web   | 350001 |          | 略   |

##### 平台工程
| 序号 | 服务名称        | 服务别名                           | 端口  | 集成情况 | 备注 |
|------|-----------------|------------------------------------|-------|----------|------|
| 1    | 注册中心        | alinesno-cloud-platform-eureka     | 24001 | 完成     |      |
| 2    | 配置服务        | ~~alinesno-cloud-platform-config~~ | 24002 |          | 删除 |
| 3    | SpringBootAdmin | alinesno-cloud-platform-admin      | 24003 |          |      |
| 4    | 熔断器多台      | alinesno-cloud-platform-turbine    | 24004 |          |      |
| 5    | 熔断器          | alinesno-cloud-platform-hysrix     | 24005 |          |      |
| 6    | 单点登陆系统    | alinesno-cloud-platform-cas        | 24006 | 完成     |      |
| 7    | 在线代码生成器  | alinesno-cloud-platform-generator  | 24007 |          | 略   |

##### 基础服务
| 序号 | 服务名称     | 服务别名                     | 端口 | 集成情况 | 备注 |
|------|--------------|------------------------------|------|----------|------|
| 1    | 基础公共服务 | alinesno-cloud-base-boot     |      | 集成中   |      |
| 2    | 内容服务     | alinesno-cloud-base-content  |      | 集成中   |      |
| 3    | 通知服务     | alinesno-cloud-base-notice   |      | 集成中   |      |
| 3    | 消息服务     | alinesno-cloud-base-message  |      | 集成中   |      |
| 4    | 支付服务     | alinesno-cloud-base-pay      |      |          |      |
| 5    | 微信服务     | alinesno-cloud-base-wechat   |      | 集成中   |      |
| 6    | 日志服务     | alinesno-cloud-base-logger   |      | 集成中   |      |
| 7    | 工作流服务   | alinesno-cloud-base-workflow |      |          |      |
| 8    | 任务服务     | alinesno-cloud-base-task     |      |          |      |
| 9    | 打印签名服务 | alinesno-cloud-base-print    |      | 集成中   | 略   |

### 门户服务
| 序号 | 服务名称 | 服务别名                      | 端口 | 集成情况 | 备注 |
|------|----------|-------------------------------|------|----------|------|
| 1    | 桌面服务 | alinesno-cloud-portal-desktop |      |          |      |
| 2    | 门户服务 | alinesno-cloud-portal-site    |      |          | 略   |

##### 网关服务
| 序号 | 服务名称 | 服务别名                | 端口 | 集成情况 | 备注 |
|------|----------|-------------------------|------|----------|------|
| 1    | 网关服务 | alinesno-cloud-gate-app |      |          |      |

##### 应用服务
| 序号 | 服务名称     | 服务别名                   | 端口 | 集成情况 | 备注 |
|------|--------------|----------------------------|------|----------|------|
| 1    | 单点应用     | ~~alinesno-cloud-web-sso~~ |      |          | 删除 |
| 2    | 门户应用     | alinesno-cloud-web-desktop |      |          |      |
| 3    | 公共管理应用 | alinesno-cloud-web-boot    |      | 集成中   | 略   |

##### 监控服务
| 序号 | 服务名称     | 服务别名                     | 端口 | 集成情况 | 备注 |
|------|--------------|------------------------------|------|----------|------|
| 1    | 监控收集应用 | alinesno-cloud-monitor-agent |      |          |      |
| 2    | 监控应用     | alinesno-cloud-monitor-web   |      |          | 略   |

##### 示例服务
| 序号 | 服务名称         | 服务别名                            | 端口  | 集成情况 | 备注 |
|------|------------------|-------------------------------------|-------|----------|------|
| 1    | 代码生成器示例   | alinesno-cloud-demo-generator       |       | 集成中   |      |
| 1    | 日志基础服务     | alinesno-cloud-demo-base            | 30001 | 集成中   |      |
| 2    | 日志基础服务接口 | alinesno-cloud-demo-base-facade     |       | 集成中   |      |
| 3    | 客户管理服务     | alinesno-cloud-demo-business        | 30002 | 集成中   |      |
| 4    | 客户管理服务接口 | alinesno-cloud-demo-business-facade |       |          |      |
| 5    | 网关服务         | alinesno-cloud-demo-gateway         | 30003 | 集成中   |      |
| 6    | 客户管理系统     | alinesno-cloud-demo-web             | 30004 |          |      |
| 1    | 示例单体应用     | alinesno-cloud-demo-single          | 30005 |          | 略   |


