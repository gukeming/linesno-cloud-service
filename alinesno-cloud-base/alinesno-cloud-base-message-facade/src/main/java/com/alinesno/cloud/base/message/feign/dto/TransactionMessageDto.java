package com.alinesno.cloud.base.message.feign.dto;

import com.alinesno.cloud.common.facade.feign.BaseDto;
import java.util.Date;

/**
 * <p> 传输对象</p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:25:27
 */
@SuppressWarnings("serial")
public class TransactionMessageDto extends BaseDto {

	private Integer areadlyDead;
	
	private String businessBody;
	
	private String businessDateType;
	
	private String businessId;
	
	private String consumerQueue;
	
	private Date createTime;
	
	private String creater;
	
	private Date editTime;
	
	private String editor;
	
	private String field1;
	
	private String field2;
	
	private String field3;
	
	private Integer messageSendTimes;
	
	private String messageStatus;
	
	private String remark;
	
	private Integer versions;
	


	public Integer getAreadlyDead() {
		return areadlyDead;
	}

	public void setAreadlyDead(Integer areadlyDead) {
		this.areadlyDead = areadlyDead;
	}

	public String getBusinessBody() {
		return businessBody;
	}

	public void setBusinessBody(String businessBody) {
		this.businessBody = businessBody;
	}

	public String getBusinessDateType() {
		return businessDateType;
	}

	public void setBusinessDateType(String businessDateType) {
		this.businessDateType = businessDateType;
	}

	public String getBusinessId() {
		return businessId;
	}

	public void setBusinessId(String businessId) {
		this.businessId = businessId;
	}

	public String getConsumerQueue() {
		return consumerQueue;
	}

	public void setConsumerQueue(String consumerQueue) {
		this.consumerQueue = consumerQueue;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getCreater() {
		return creater;
	}

	public void setCreater(String creater) {
		this.creater = creater;
	}

	public Date getEditTime() {
		return editTime;
	}

	public void setEditTime(Date editTime) {
		this.editTime = editTime;
	}

	public String getEditor() {
		return editor;
	}

	public void setEditor(String editor) {
		this.editor = editor;
	}

	public String getField1() {
		return field1;
	}

	public void setField1(String field1) {
		this.field1 = field1;
	}

	public String getField2() {
		return field2;
	}

	public void setField2(String field2) {
		this.field2 = field2;
	}

	public String getField3() {
		return field3;
	}

	public void setField3(String field3) {
		this.field3 = field3;
	}

	public Integer getMessageSendTimes() {
		return messageSendTimes;
	}

	public void setMessageSendTimes(Integer messageSendTimes) {
		this.messageSendTimes = messageSendTimes;
	}

	public String getMessageStatus() {
		return messageStatus;
	}

	public void setMessageStatus(String messageStatus) {
		this.messageStatus = messageStatus;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Integer getVersions() {
		return versions;
	}

	public void setVersions(Integer versions) {
		this.versions = versions;
	}

}
