package com.alinesno.cloud.base.boot.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alinesno.cloud.base.boot.entity.ManagerAccountEntity;
import com.alinesno.cloud.base.boot.entity.ManagerRoleEntity;
import com.alinesno.cloud.base.boot.service.IManagerRoleService;
import com.alinesno.cloud.common.core.rest.BaseRestController;

/**
 * <p> 接口 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 17:53:19
 */
@Scope("prototype")
@RestController
@RequestMapping("managerRole")
public class ManagerRoleRestController extends BaseRestController<ManagerRoleEntity , IManagerRoleService> {

	//日志记录
	@SuppressWarnings("unused")
	private final static Logger logger = LoggerFactory.getLogger(ManagerRoleRestController.class);
	/**
	 * 保存用户权限
	 * @param managerRoleDto
	 * @param functiondIds
	 * @return
	 */
	@PostMapping("saveRole")
	boolean saveRole(@RequestBody ManagerRoleEntity managerRoleEntity, @RequestParam("functionIds") String functionIds) {
		return service.saveRole(managerRoleEntity , functionIds) ; 
	}
	
	/**
	 * 用户授权
	 * @param accountId
	 * @param rolesId
	 * @return
	 */
	@PostMapping("authAccount")
	boolean authAccount(@RequestBody ManagerAccountEntity accountEntity , @RequestParam("rolesId") String rolesId) {
		return service.authAccount(accountEntity , rolesId) ; 
	}

	/**
	 * 查询用户所有角色
	 * @param accountId
	 * @return
	 */
	@GetMapping("findByAccountId")
	List<ManagerRoleEntity> findByAccountId(@RequestParam("accountId")  String accountId){
		return service.findByAccountId(accountId) ; 
	}

}
