package com.alinesno.cloud.base.boot.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.alinesno.cloud.common.core.orm.entity.BaseEntity;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 17:53:19
 */
@Entity
@Table(name="info_address")
public class InfoAddressEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

	private String owners;


	public String getOwners() {
		return owners;
	}

	public void setOwners(String owners) {
		this.owners = owners;
	}


	@Override
	public String toString() {
		return "InfoAddressEntity{" +
			"owners=" + owners +
			"}";
	}
}
