package com.alinesno.cloud.base.boot.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alinesno.cloud.base.boot.entity.ManagerCodeTypeEntity;
import com.alinesno.cloud.base.boot.service.IManagerCodeTypeService;
import com.alinesno.cloud.common.core.rest.BaseRestController;

/**
 * <p> 接口 </p>
 *
 * @author LuoAnDong
 * @since 2019-02-07 21:16:11
 */
@Scope("prototype")
@RestController
@RequestMapping("managerCodeType")
public class ManagerCodeTypeRestController extends BaseRestController<ManagerCodeTypeEntity , IManagerCodeTypeService> {

	//日志记录
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(ManagerCodeTypeRestController.class);

	
	/**
	 * 通过代码类型查询代码
	 * @param asText
	 * @return
	 */
	@GetMapping("findByCodeTypeValue")
	ManagerCodeTypeEntity findByCodeTypeValue(String codeTypeValue) {
		return service.findByCodeTypeValue(codeTypeValue) ; 
	}

}
