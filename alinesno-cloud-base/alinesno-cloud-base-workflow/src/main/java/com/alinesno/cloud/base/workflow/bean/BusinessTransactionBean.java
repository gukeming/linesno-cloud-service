package com.alinesno.cloud.base.workflow.bean;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class BusinessTransactionBean implements Serializable {
	private static final long serialVersionUID = 7336604915590955343L;
	/**
	 * 流程定义ID
	 */
	private long processDefID;
	/**
	 * 前置活动定义ID
	 */
	private String beginActId;
	/**
	 * 后置活动定义id
	 */
	private String activityDefID;
	/**
	 * 活动实例ID
	 */
	long activityInstID;
	/**
	 * 活动实例名称
	 */
	String activityInstName;
	/**
	 * 活动工作项ID
	 */
	private Long workItemID;
	/**
	 * 参与者名称
	 */
	private String partiName;
	/**
	 * 活动操作工作项
	 */
	private WorkItemBean workItem;

	public BusinessTransactionBean() {
	}

	public BusinessTransactionBean(long processDefID, String activityDefID) {
		super();
		this.processDefID = processDefID;
		this.activityDefID = activityDefID;
	}

	public BusinessTransactionBean(long processDefID, String activityDefID, Long workItemID) {
		super();
		this.processDefID = processDefID;
		this.activityDefID = activityDefID;
		this.workItemID = workItemID;
	}

	public long getProcessDefID() {
		return processDefID;
	}

	public void setProcessDefID(long processDefID) {
		this.processDefID = processDefID;
	}

	public String getActivityDefID() {
		return activityDefID;
	}

	public void setActivityDefID(String activityDefID) {
		this.activityDefID = activityDefID;
	}

	public Long getWorkItemID() {
		return workItemID;
	}

	public void setWorkItemID(Long workItemID) {
		this.workItemID = workItemID;
	}

	public String getBeginActId() {
		return beginActId;
	}

	public void setBeginActId(String beginActId) {
		this.beginActId = beginActId;
	}

	public WorkItemBean getWorkItem() {
		return workItem;
	}

	public void setWorkItem(WorkItemBean workItem) {
		this.workItem = workItem;
	}

	public String getPartiName() {
		return partiName;
	}

	public void setPartiName(String partiName) {
		this.partiName = partiName;
	}

	public long getActivityInstID() {
		return activityInstID;
	}

	public void setActivityInstID(long activityInstID) {
		this.activityInstID = activityInstID;
	}

	public String getActivityInstName() {
		return activityInstName;
	}

	public void setActivityInstName(String activityInstName) {
		this.activityInstName = activityInstName;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
