package com.alinesno.cloud.base.workflow.bean;

public class ApproveBean {
	/**
	 * 上报人
	 */
	private String czy;
	/**
	 * 经办机构编号
	 */
	private String jbjgbh;
	/**
	 * 中心代码
	 */
	private String zxdm;
	/**
	 * 流程定义名称
	 */
	private String processDefName;

	public String getJbjgbh() {
		return this.jbjgbh;
	}

	public void setJbjgbh(String jbjgbh) {
		this.jbjgbh = jbjgbh;
	}

	public String getCzy() {
		return this.czy;
	}

	public void setCzy(String czy) {
		this.czy = czy;
	}

	public String getZxdm() {
		return zxdm;
	}

	public void setZxdm(String zxdm) {
		this.zxdm = zxdm;
	}

	public String getProcessDefName() {
		return processDefName;
	}

	public void setProcessDefName(String processDefName) {
		this.processDefName = processDefName;
	}

}
