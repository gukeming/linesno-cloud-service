package com.alinesno.cloud.base.notice.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.alinesno.cloud.common.core.orm.entity.BaseEntity;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2018-12-02 16:08:00
 */
@Entity
@Table(name="email_template")
public class EmailTemplateEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 模板名称
     */
	@Column(name="template_name")
	private String templateName;
	@Column(name="template_content")
	private String templateContent;
    /**
     * 模板使用时间 
     */
	@Column(name="template_use_time")
	private Date templateUseTime;
    /**
     * 模板版本号(10代表1.0版本,没有小版本,即没有111位)
     */
	@Column(name="template_version")
	private Integer templateVersion;
    /**
     * 所属应用
     */
	@Column(name="application_id")
	private String applicationId;
    /**
     * 所属租户
     */
	@Column(name="tenant_id")
	private String tenantId;


	public String getTemplateName() {
		return templateName;
	}

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	public String getTemplateContent() {
		return templateContent;
	}

	public void setTemplateContent(String templateContent) {
		this.templateContent = templateContent;
	}

	public Date getTemplateUseTime() {
		return templateUseTime;
	}

	public void setTemplateUseTime(Date templateUseTime) {
		this.templateUseTime = templateUseTime;
	}

	public Integer getTemplateVersion() {
		return templateVersion;
	}

	public void setTemplateVersion(Integer templateVersion) {
		this.templateVersion = templateVersion;
	}

	public String getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}


	@Override
	public String toString() {
		return "EmailTemplateEntity{" +
			"templateName=" + templateName +
			", templateContent=" + templateContent +
			", templateUseTime=" + templateUseTime +
			", templateVersion=" + templateVersion +
			", applicationId=" + applicationId +
			", tenantId=" + tenantId +
			"}";
	}
}
