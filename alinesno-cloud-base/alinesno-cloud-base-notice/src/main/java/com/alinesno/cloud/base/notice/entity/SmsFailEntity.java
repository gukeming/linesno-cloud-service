package com.alinesno.cloud.base.notice.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.alinesno.cloud.common.core.orm.entity.BaseEntity;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2018-12-02 16:08:00
 */
@Entity
@Table(name="sms_fail")
public class SmsFailEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 所属应用
     */
	@Column(name="application_id")
	private String applicationId;
    /**
     * 所属租户
     */
	@Column(name="tenant_id")
	private String tenantId;
    /**
     * 验证码
     */
	@Column(name="validate_code")
	private String validateCode;
    /**
     * 短信内容 
     */
	private String content;
    /**
     * 业务id
     */
	@Column(name="business_id")
	private String businessId;
    /**
     * 短信签名
     */
	@Column(name="sign_name")
	private String signName;
    /**
     * 模板代码
     */
	@Column(name="template_code")
	private String templateCode;
    /**
     * 模板内容
     */
	private String template;
    /**
     * 手机号码 
     */
	private String phone;
    /**
     * 扩展id
     */
	@Column(name="out_id")
	private String outId;
    /**
     * 返回的业务id
     */
	@Column(name="biz_id")
	private String bizId;


	public String getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public String getValidateCode() {
		return validateCode;
	}

	public void setValidateCode(String validateCode) {
		this.validateCode = validateCode;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getBusinessId() {
		return businessId;
	}

	public void setBusinessId(String businessId) {
		this.businessId = businessId;
	}

	public String getSignName() {
		return signName;
	}

	public void setSignName(String signName) {
		this.signName = signName;
	}

	public String getTemplateCode() {
		return templateCode;
	}

	public void setTemplateCode(String templateCode) {
		this.templateCode = templateCode;
	}

	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getOutId() {
		return outId;
	}

	public void setOutId(String outId) {
		this.outId = outId;
	}

	public String getBizId() {
		return bizId;
	}

	public void setBizId(String bizId) {
		this.bizId = bizId;
	}


	@Override
	public String toString() {
		return "SmsFailEntity{" +
			"applicationId=" + applicationId +
			", tenantId=" + tenantId +
			", validateCode=" + validateCode +
			", content=" + content +
			", businessId=" + businessId +
			", signName=" + signName +
			", templateCode=" + templateCode +
			", template=" + template +
			", phone=" + phone +
			", outId=" + outId +
			", bizId=" + bizId +
			"}";
	}
}
