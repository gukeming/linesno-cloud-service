package com.alinesno.cloud.base.notice.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.alinesno.cloud.common.core.orm.entity.BaseEntity;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2018-12-02 16:08:00
 */
@Entity
@Table(name="email_task")
public class EmailTaskEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 所属应用
     */
	@Column(name="application_id")
	private String applicationId;
    /**
     * 所属租户
     */
	@Column(name="tenant_id")
	private String tenantId;


	public String getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}


	@Override
	public String toString() {
		return "EmailTaskEntity{" +
			"applicationId=" + applicationId +
			", tenantId=" + tenantId +
			"}";
	}
}
