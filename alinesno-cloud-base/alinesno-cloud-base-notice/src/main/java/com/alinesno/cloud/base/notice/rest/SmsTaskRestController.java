package com.alinesno.cloud.base.notice.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alinesno.cloud.base.notice.entity.SmsTaskEntity;
import com.alinesno.cloud.base.notice.service.ISmsTaskService;
import com.alinesno.cloud.common.core.rest.BaseRestController;

/**
 * <p> 接口 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-02 16:08:00
 */
@Scope("prototype")
@RestController
@RequestMapping("smsTask")
public class SmsTaskRestController extends BaseRestController<SmsTaskEntity , ISmsTaskService> {

	//日志记录
	@SuppressWarnings("unused")
	private final static Logger logger = LoggerFactory.getLogger(SmsTaskRestController.class);

}
