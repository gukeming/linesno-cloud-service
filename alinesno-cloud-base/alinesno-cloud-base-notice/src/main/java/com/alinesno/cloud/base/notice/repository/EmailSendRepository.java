package com.alinesno.cloud.base.notice.repository;

import com.alinesno.cloud.base.notice.entity.EmailSendEntity;
import com.alinesno.cloud.common.core.orm.repository.IBaseJpaRepository;

/**
 * <p>
  *  持久层接口
 * </p>
 *
 * @author LuoAnDong
 * @since 2018-12-02 16:08:00
 */
public interface EmailSendRepository extends IBaseJpaRepository<EmailSendEntity, String> {

}
