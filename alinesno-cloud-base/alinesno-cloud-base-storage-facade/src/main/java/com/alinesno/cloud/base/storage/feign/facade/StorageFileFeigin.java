package com.alinesno.cloud.base.storage.feign.facade;

import org.springframework.cloud.openfeign.FeignClient;

import com.alinesno.cloud.base.storage.feign.dto.StorageFileDto;
import com.alinesno.cloud.common.facade.feign.IBaseFeign;

/**
 * <p>  请求客户端 </p>
 *
 * @author LuoAnDong
 * @since 2019-04-09 20:22:17
 */
@FeignClient(name="alinesno-cloud-base-storage" , path="storageFile")
public interface StorageFileFeigin extends IBaseFeign<StorageFileDto> {


	
}
