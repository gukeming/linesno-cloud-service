package com.alinesno.cloud.base.storage.api;

import java.io.File;
import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import com.alinesno.cloud.base.storage.enums.StorageStrategyEnums;
import com.alinesno.cloud.base.storage.feign.dto.StorageFileDto;
import com.google.gson.Gson;

/**
 * 上传请求客户端
 * 
 * @author LuoAnDong
 * @since 2019年4月11日 上午7:19:10
 */
@Component
public class StorageFileClient {

	private static final Logger log = LoggerFactory.getLogger(StorageFileClient.class);
	private Gson gson = new Gson() ; 

	/**
	 * 负载地址 
	 */
	@Value("${storage.load-load}")
	private String loadPath ; 
	
	/**
	 * 文件上传
	 * 
	 * @param fileLoalAbcPath
	 * @param strategy
	 * @return
	 * @throws IOException
	 * @throws ClientProtocolException
	 */
	public StorageFileDto uploadData(String localFile, StorageStrategyEnums strategyEnmnus) throws ClientProtocolException, IOException {
		Assert.hasLength(loadPath , "上传负载路径不能为空.");
		
		log.debug("localFile:{} , strategyEnmnus:{}", localFile, strategyEnmnus);
		CloseableHttpClient httpclient = HttpClients.createDefault();
		try {
			loadPath = loadPath.endsWith("/")?loadPath:loadPath+"/" ; 
			HttpPost httppost = new HttpPost(loadPath+"storageFile/uploadFile");
			MultipartEntityBuilder builder = MultipartEntityBuilder.create() ; 
			
			FileBody bin = new FileBody(new File(localFile));
			builder.addPart("file", bin) ; 
			
			if(strategyEnmnus != null) {
				StringBody strategy = new StringBody(strategyEnmnus.value(), ContentType.TEXT_PLAIN);
				builder.addPart("strategy", strategy) ; 
			}
			HttpEntity reqEntity = builder.build() ; 

			httppost.setEntity(reqEntity);

			CloseableHttpResponse response = httpclient.execute(httppost);
			try {
				HttpEntity resEntity = response.getEntity();
				if (resEntity != null) {

					StatusLine statusLine = response.getStatusLine();
					int statusCode = statusLine.getStatusCode();
					if (statusCode == 200) { /** 获取服务器返回的文本消息 */
						HttpEntity httpEntity = response.getEntity();
						String feedback = EntityUtils.toString(httpEntity, "UTF-8");
						
						log.debug("feedback:{}", feedback);
						return gson.fromJson(feedback, StorageFileDto.class) ; 
					}

				}
				EntityUtils.consume(resEntity);
				resEntity.getContent();
			} finally {
				response.close();
			}
		} finally {
			httpclient.close();
		}

		return null;
	}

	/**
	 * 文件上传
	 * 
	 * @param fileLoalAbcPath
	 * @param strategy
	 * @return
	 * @throws IOException 
	 * @throws ClientProtocolException 
	 */
	public StorageFileDto uploadData(String localFile) throws ClientProtocolException, IOException {
		return this.uploadData(localFile, null);
	}

	/**
	 * 获取文件下载链接
	 * 
	 * @param fileId 文件保存id
	 * @return
	 */
	public String downloadData(String fileId) {

		return null;
	}

	/**
	 * 删除文件
	 * 
	 * @param fileId
	 * @return
	 */
	public boolean deleteData(String fileId) {

		return false;
	}

}
