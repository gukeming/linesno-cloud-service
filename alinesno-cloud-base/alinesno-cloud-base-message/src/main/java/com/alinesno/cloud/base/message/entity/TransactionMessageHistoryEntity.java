package com.alinesno.cloud.base.message.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.alinesno.cloud.common.core.orm.entity.BaseEntity;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:25:27
 */
@Entity
@Table(name="transaction_message_history")
public class TransactionMessageHistoryEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

	@Column(name="areadly_dead")
	private Integer areadlyDead;
	@Column(name="business_body")
	private String businessBody;
	@Column(name="business_date_type")
	private String businessDateType;
	@Column(name="business_id")
	private String businessId;
	@Column(name="consumer_queue")
	private String consumerQueue;
	@Column(name="create_time")
	private Date createTime;
	private String creater;
	@Column(name="edit_time")
	private Date editTime;
	private String editor;
	private String field1;
	private String field2;
	private String field3;
	@Column(name="message_send_times")
	private Integer messageSendTimes;
	@Column(name="message_status")
	private String messageStatus;
	private String remark;
	private Integer versions;


	public Integer getAreadlyDead() {
		return areadlyDead;
	}

	public void setAreadlyDead(Integer areadlyDead) {
		this.areadlyDead = areadlyDead;
	}

	public String getBusinessBody() {
		return businessBody;
	}

	public void setBusinessBody(String businessBody) {
		this.businessBody = businessBody;
	}

	public String getBusinessDateType() {
		return businessDateType;
	}

	public void setBusinessDateType(String businessDateType) {
		this.businessDateType = businessDateType;
	}

	public String getBusinessId() {
		return businessId;
	}

	public void setBusinessId(String businessId) {
		this.businessId = businessId;
	}

	public String getConsumerQueue() {
		return consumerQueue;
	}

	public void setConsumerQueue(String consumerQueue) {
		this.consumerQueue = consumerQueue;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getCreater() {
		return creater;
	}

	public void setCreater(String creater) {
		this.creater = creater;
	}

	public Date getEditTime() {
		return editTime;
	}

	public void setEditTime(Date editTime) {
		this.editTime = editTime;
	}

	public String getEditor() {
		return editor;
	}

	public void setEditor(String editor) {
		this.editor = editor;
	}

	public String getField1() {
		return field1;
	}

	public void setField1(String field1) {
		this.field1 = field1;
	}

	public String getField2() {
		return field2;
	}

	public void setField2(String field2) {
		this.field2 = field2;
	}

	public String getField3() {
		return field3;
	}

	public void setField3(String field3) {
		this.field3 = field3;
	}

	public Integer getMessageSendTimes() {
		return messageSendTimes;
	}

	public void setMessageSendTimes(Integer messageSendTimes) {
		this.messageSendTimes = messageSendTimes;
	}

	public String getMessageStatus() {
		return messageStatus;
	}

	public void setMessageStatus(String messageStatus) {
		this.messageStatus = messageStatus;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Integer getVersions() {
		return versions;
	}

	public void setVersions(Integer versions) {
		this.versions = versions;
	}


	@Override
	public String toString() {
		return "TransactionMessageHistoryEntity{" +
			"areadlyDead=" + areadlyDead +
			", businessBody=" + businessBody +
			", businessDateType=" + businessDateType +
			", businessId=" + businessId +
			", consumerQueue=" + consumerQueue +
			", createTime=" + createTime +
			", creater=" + creater +
			", editTime=" + editTime +
			", editor=" + editor +
			", field1=" + field1 +
			", field2=" + field2 +
			", field3=" + field3 +
			", messageSendTimes=" + messageSendTimes +
			", messageStatus=" + messageStatus +
			", remark=" + remark +
			", versions=" + versions +
			"}";
	}
}
