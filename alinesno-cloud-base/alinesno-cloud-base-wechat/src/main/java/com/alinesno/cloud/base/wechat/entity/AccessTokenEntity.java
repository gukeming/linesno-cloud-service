package com.alinesno.cloud.base.wechat.entity;

import javax.persistence.Column;
import javax.persistence.Table;
import javax.persistence.Entity;
import com.alinesno.cloud.common.core.orm.entity.BaseEntity;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:11:54
 */
@Entity
@Table(name="access_token")
public class AccessTokenEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 微信accessToken
     */
	@Column(name="access_token")
	private String accessToken;
    /**
     * 错误代码
     */
	private String errcode;
    /**
     * 错误信息
     */
	private String errmsg;
    /**
     * token超时时间
     */
	@Column(name="expires_in")
	private String expiresIn;
    /**
     * token唯一id号
     */
	@Column(name="token_id")
	private String tokenId;


	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getErrcode() {
		return errcode;
	}

	public void setErrcode(String errcode) {
		this.errcode = errcode;
	}

	public String getErrmsg() {
		return errmsg;
	}

	public void setErrmsg(String errmsg) {
		this.errmsg = errmsg;
	}

	public String getExpiresIn() {
		return expiresIn;
	}

	public void setExpiresIn(String expiresIn) {
		this.expiresIn = expiresIn;
	}

	public String getTokenId() {
		return tokenId;
	}

	public void setTokenId(String tokenId) {
		this.tokenId = tokenId;
	}


	@Override
	public String toString() {
		return "AccessTokenEntity{" +
			"accessToken=" + accessToken +
			", errcode=" + errcode +
			", errmsg=" + errmsg +
			", expiresIn=" + expiresIn +
			", tokenId=" + tokenId +
			"}";
	}
}
