package com.alinesno.cloud.base.wechat.service;

import org.springframework.data.repository.NoRepositoryBean;
import com.alinesno.cloud.base.wechat.entity.ActionEntity;
import com.alinesno.cloud.common.core.services.IBaseService;
import com.alinesno.cloud.base.wechat.repository.ActionRepository;

/**
 * <p>  服务类 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:11:54
 */
@NoRepositoryBean
public interface IActionService extends IBaseService<ActionRepository, ActionEntity, String> {

}
