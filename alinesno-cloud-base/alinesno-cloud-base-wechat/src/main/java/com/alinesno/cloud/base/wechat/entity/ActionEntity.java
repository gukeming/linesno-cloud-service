package com.alinesno.cloud.base.wechat.entity;

import javax.persistence.Column;
import com.alinesno.cloud.common.core.orm.entity.BaseEntity;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:11:54
 */
public class ActionEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 文本消息内容
     */
	private String content;
    /**
     * 消息创建时间 （整型）
     */
	@Column(name="create_time")
	private String createTime;
    /**
     * 事件类型，subscribe(订阅)、unsubscribe(取消订阅)
     */
	private String event;
    /**
     * 语音格式，如amr，speex等
     */
	private String format;
    /**
     * 发送方帐号（一个OpenID）
     */
	@Column(name="from_user_name")
	private String fromUserName;
    /**
     * 语音消息媒体id，可以调用多媒体文件下载接口拉取数据。
     */
	@Column(name="media_id")
	private String mediaId;
    /**
     * 消息id，64位整型
     */
	@Column(name="msg_id")
	private String msgId;
    /**
     * 消息类型，event
     */
	@Column(name="msg_type")
	private String msgType;
    /**
     * 发送状态为成功
     */
	private String status;
    /**
     * 二维码的ticket，可用来换取二维码图片 ;
     */
	private String ticket;
    /**
     * 开发者微信号
     */
	@Column(name="to_user_name")
	private String toUserName;
    /**
     * 响应id
     */
	@Column(name="action_id")
	private String actionId;


	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public String getFromUserName() {
		return fromUserName;
	}

	public void setFromUserName(String fromUserName) {
		this.fromUserName = fromUserName;
	}

	public String getMediaId() {
		return mediaId;
	}

	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}

	public String getMsgId() {
		return msgId;
	}

	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTicket() {
		return ticket;
	}

	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

	public String getToUserName() {
		return toUserName;
	}

	public void setToUserName(String toUserName) {
		this.toUserName = toUserName;
	}

	public String getActionId() {
		return actionId;
	}

	public void setActionId(String actionId) {
		this.actionId = actionId;
	}


	@Override
	public String toString() {
		return "ActionEntity{" +
			"content=" + content +
			", createTime=" + createTime +
			", event=" + event +
			", format=" + format +
			", fromUserName=" + fromUserName +
			", mediaId=" + mediaId +
			", msgId=" + msgId +
			", msgType=" + msgType +
			", status=" + status +
			", ticket=" + ticket +
			", toUserName=" + toUserName +
			", actionId=" + actionId +
			"}";
	}
}
