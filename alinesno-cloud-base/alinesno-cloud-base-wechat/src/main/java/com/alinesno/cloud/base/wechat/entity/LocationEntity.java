package com.alinesno.cloud.base.wechat.entity;

import javax.persistence.Column;
import com.alinesno.cloud.common.core.orm.entity.BaseEntity;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:11:54
 */
public class LocationEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 消息创建时间 （整型）
     */
	@Column(name="create_time")
	private String createTime;
    /**
     * 事件类型，subscribe(订阅)、unsubscribe(取消订阅)
     */
	private String event;
    /**
     * 发送方帐号（一个OpenID）
     */
	@Column(name="from_user_name")
	private String fromUserName;
    /**
     * 地理位置纬度
     */
	private String latitude;
    /**
     * 地理位置经度
     */
	private String longitude;
    /**
     * 消息类型，event
     */
	@Column(name="msg_type")
	private String msgType;
    /**
     * 地理位置精度
     */
	private String precisions;
    /**
     * 开发者微信号
     */
	@Column(name="to_user_name")
	private String toUserName;
	@Column(name="location_id")
	private String locationId;


	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public String getFromUserName() {
		return fromUserName;
	}

	public void setFromUserName(String fromUserName) {
		this.fromUserName = fromUserName;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public String getPrecisions() {
		return precisions;
	}

	public void setPrecisions(String precisions) {
		this.precisions = precisions;
	}

	public String getToUserName() {
		return toUserName;
	}

	public void setToUserName(String toUserName) {
		this.toUserName = toUserName;
	}

	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}


	@Override
	public String toString() {
		return "LocationEntity{" +
			"createTime=" + createTime +
			", event=" + event +
			", fromUserName=" + fromUserName +
			", latitude=" + latitude +
			", longitude=" + longitude +
			", msgType=" + msgType +
			", precisions=" + precisions +
			", toUserName=" + toUserName +
			", locationId=" + locationId +
			"}";
	}
}
