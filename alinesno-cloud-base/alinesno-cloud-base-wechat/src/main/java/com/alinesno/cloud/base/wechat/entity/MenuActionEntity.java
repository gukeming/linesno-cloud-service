package com.alinesno.cloud.base.wechat.entity;

import javax.persistence.Column;
import javax.persistence.Table;
import javax.persistence.Entity;
import com.alinesno.cloud.common.core.orm.entity.BaseEntity;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:11:54
 */
@Entity
@Table(name="menu_action")
public class MenuActionEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 消息创建时间 （整型）
     */
	@Column(name="create_time")
	private String createTime;
    /**
     * 事件类型，subscribe(订阅)、unsubscribe(取消订阅)
     */
	private String event;
    /**
     * 事件KEY值，设置的跳转URL
     */
	@Column(name="event_key")
	private String eventKey;
    /**
     * 发送方帐号（一个OpenID）
     */
	@Column(name="from_user_name")
	private String fromUserName;
    /**
     * 菜单id
     */
	@Column(name="menu_id")
	private String menuId;
    /**
     * 消息类型，event
     */
	@Column(name="msg_type")
	private String msgType;
    /**
     * 开发者微信号
     */
	@Column(name="to_user_name")
	private String toUserName;
	@Column(name="action_id")
	private String actionId;


	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public String getEventKey() {
		return eventKey;
	}

	public void setEventKey(String eventKey) {
		this.eventKey = eventKey;
	}

	public String getFromUserName() {
		return fromUserName;
	}

	public void setFromUserName(String fromUserName) {
		this.fromUserName = fromUserName;
	}

	public String getMenuId() {
		return menuId;
	}

	public void setMenuId(String menuId) {
		this.menuId = menuId;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public String getToUserName() {
		return toUserName;
	}

	public void setToUserName(String toUserName) {
		this.toUserName = toUserName;
	}

	public String getActionId() {
		return actionId;
	}

	public void setActionId(String actionId) {
		this.actionId = actionId;
	}


	@Override
	public String toString() {
		return "MenuActionEntity{" +
			"createTime=" + createTime +
			", event=" + event +
			", eventKey=" + eventKey +
			", fromUserName=" + fromUserName +
			", menuId=" + menuId +
			", msgType=" + msgType +
			", toUserName=" + toUserName +
			", actionId=" + actionId +
			"}";
	}
}
