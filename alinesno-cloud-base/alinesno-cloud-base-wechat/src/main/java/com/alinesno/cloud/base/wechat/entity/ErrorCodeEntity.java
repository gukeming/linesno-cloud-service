package com.alinesno.cloud.base.wechat.entity;

import javax.persistence.Column;
import javax.persistence.Table;
import javax.persistence.Entity;
import com.alinesno.cloud.common.core.orm.entity.BaseEntity;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:11:54
 */
@Entity
@Table(name="error_code")
public class ErrorCodeEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 错误代码
     */
	private String errcode;
    /**
     * 错误的id
     */
	@Column(name="error_id")
	private String errorId;
    /**
     * 错误说明
     */
	@Column(name="error_msg")
	private String errorMsg;
    /**
     * 错误类型
     */
	@Column(name="error_type")
	private String errorType;


	public String getErrcode() {
		return errcode;
	}

	public void setErrcode(String errcode) {
		this.errcode = errcode;
	}

	public String getErrorId() {
		return errorId;
	}

	public void setErrorId(String errorId) {
		this.errorId = errorId;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public String getErrorType() {
		return errorType;
	}

	public void setErrorType(String errorType) {
		this.errorType = errorType;
	}


	@Override
	public String toString() {
		return "ErrorCodeEntity{" +
			"errcode=" + errcode +
			", errorId=" + errorId +
			", errorMsg=" + errorMsg +
			", errorType=" + errorType +
			"}";
	}
}
