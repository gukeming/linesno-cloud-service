package com.alinesno.cloud.base.wechat.service;

import org.springframework.data.repository.NoRepositoryBean;
import com.alinesno.cloud.base.wechat.entity.OauthAccessTokenEntity;
import com.alinesno.cloud.common.core.services.IBaseService;
import com.alinesno.cloud.base.wechat.repository.OauthAccessTokenRepository;

/**
 * <p>  服务类 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:11:54
 */
@NoRepositoryBean
public interface IOauthAccessTokenService extends IBaseService<OauthAccessTokenRepository, OauthAccessTokenEntity, String> {

}
