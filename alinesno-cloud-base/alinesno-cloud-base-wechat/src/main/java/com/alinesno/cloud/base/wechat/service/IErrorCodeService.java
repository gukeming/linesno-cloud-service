package com.alinesno.cloud.base.wechat.service;

import org.springframework.data.repository.NoRepositoryBean;
import com.alinesno.cloud.base.wechat.entity.ErrorCodeEntity;
import com.alinesno.cloud.common.core.services.IBaseService;
import com.alinesno.cloud.base.wechat.repository.ErrorCodeRepository;

/**
 * <p>  服务类 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:11:54
 */
@NoRepositoryBean
public interface IErrorCodeService extends IBaseService<ErrorCodeRepository, ErrorCodeEntity, String> {

}
