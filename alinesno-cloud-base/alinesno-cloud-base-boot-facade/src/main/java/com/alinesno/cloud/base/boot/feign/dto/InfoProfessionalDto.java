package com.alinesno.cloud.base.boot.feign.dto;

import com.alinesno.cloud.common.facade.feign.BaseDto;

/**
 * <p> 传输对象</p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:00:40
 */
@SuppressWarnings("serial")
public class InfoProfessionalDto extends BaseDto {

	private String owners;
	
	private String professionalName;
	
	private String professionalProp;
	


	public String getOwners() {
		return owners;
	}

	public void setOwners(String owners) {
		this.owners = owners;
	}

	public String getProfessionalName() {
		return professionalName;
	}

	public void setProfessionalName(String professionalName) {
		this.professionalName = professionalName;
	}

	public String getProfessionalProp() {
		return professionalProp;
	}

	public void setProfessionalProp(String professionalProp) {
		this.professionalProp = professionalProp;
	}

}
