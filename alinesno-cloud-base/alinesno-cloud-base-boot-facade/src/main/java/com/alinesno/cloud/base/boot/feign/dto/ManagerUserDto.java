package com.alinesno.cloud.base.boot.feign.dto;

import java.util.Date;

import com.alinesno.cloud.common.facade.feign.BaseDto;

/**
 * <p> 传输对象</p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:00:40
 */
@SuppressWarnings("serial")
public class ManagerUserDto extends BaseDto {

    /**
     * 登陆账户
     */
	private String userAccount;
	
    /**
     * 用户密码
     */
	private String userPassword;
	
    /**
     * 用户加密字段
     */
	private String userSalt;
	
    /**
     * 添加时间
     */
	private Date userAddtime;
	
    /**
     * 状态(1正常/0非法)
     */
	private Boolean userStatus;
	
    /**
     * 用户角色
     */
	private String userRole;
	
    /**
     * 用户类型(1超级管理员/0普通管理员)
     */
	private Boolean userType;
	
    /**
     * 用户名称
     */
	private String userName ;
	


	public String getUserAccount() {
		return userAccount;
	}

	public void setUserAccount(String userAccount) {
		this.userAccount = userAccount;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public String getUserSalt() {
		return userSalt;
	}

	public void setUserSalt(String userSalt) {
		this.userSalt = userSalt;
	}

	public Date getUserAddtime() {
		return userAddtime;
	}

	public void setUserAddtime(Date userAddtime) {
		this.userAddtime = userAddtime;
	}

	public Boolean isUserStatus() {
		return userStatus;
	}

	public void setUserStatus(Boolean userStatus) {
		this.userStatus = userStatus;
	}

	public String getUserRole() {
		return userRole;
	}

	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}

	public Boolean isUserType() {
		return userType;
	}

	public void setUserType(Boolean userType) {
		this.userType = userType;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Boolean getUserStatus() {
		return userStatus;
	}

	public Boolean getUserType() {
		return userType;
	}

}
