package com.alinesno.cloud.base.boot.feign.facade;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.alinesno.cloud.base.boot.feign.dto.ContentPostTypeDto;
import com.alinesno.cloud.common.facade.feign.IBaseFeign;
import com.alinesno.cloud.common.facade.wrapper.RestWrapper;

/**
 * <p>  请求客户端 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:02:27
 */
@FeignClient(name="alinesno-cloud-base-boot" , path="contentPostType")
public interface ContentPostTypeFeigin extends IBaseFeign<ContentPostTypeDto> {

	@PostMapping("findAllWithApplication")
	List<ContentPostTypeDto> findAllWithApplication(@RequestBody RestWrapper restWrapper);

}
