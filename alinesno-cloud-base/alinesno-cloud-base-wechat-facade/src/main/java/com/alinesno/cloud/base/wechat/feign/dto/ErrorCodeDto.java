package com.alinesno.cloud.base.wechat.feign.dto;

import com.alinesno.cloud.common.facade.feign.BaseDto;

/**
 * <p> 传输对象</p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:11:54
 */
@SuppressWarnings("serial")
public class ErrorCodeDto extends BaseDto {

    /**
     * 错误代码
     */
	private String errcode;
	
    /**
     * 错误的id
     */
	private String errorId;
	
    /**
     * 错误说明
     */
	private String errorMsg;
	
    /**
     * 错误类型
     */
	private String errorType;
	


	public String getErrcode() {
		return errcode;
	}

	public void setErrcode(String errcode) {
		this.errcode = errcode;
	}

	public String getErrorId() {
		return errorId;
	}

	public void setErrorId(String errorId) {
		this.errorId = errorId;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public String getErrorType() {
		return errorType;
	}

	public void setErrorType(String errorType) {
		this.errorType = errorType;
	}

}
