package com.alinesno.cloud.base.wechat.feign.dto;

import com.alinesno.cloud.common.facade.feign.BaseDto;

/**
 * <p> 传输对象</p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:11:54
 */
@SuppressWarnings("serial")
public class OrderDto extends BaseDto {

    /**
     * 发送的数据包
     */
	private String attach;
	
    /**
     * 随机字符串
     */
	private String nonceStr;
	
    /**
     * 返回的费用
     */
	private Double notifyBill;
	
    /**
     * 通知支付的时间
     */
	private String notifyTimeEnd;
	
    /**
     * 下单的用户
     */
	private String openId;
	
    /**
     * 下单的费用
     */
	private Double orderBill;
	
	private String orderId;
	
    /**
     * 订单的id
订单的id
     */
	private String outTradeNo;
	
    /**
     * 备注信息
     */
	private String payRemark;
	
    /**
     * 签名
     */
	private String paySign;
	
    /**
     * 支付的状态(0下单成功|1支付成功|2支付失败)
     */
	private String payStatus;
	
    /**
     * 预支付ID串，如: prepare_id=xxx
     */
	private String pkg;
	
    /**
     * 签名类型MD5
     */
	private String signType;
	
    /**
     * 时间戳(s)
     */
	private String timeStamp;
	
    /**
     * 微信支付订单号
     */
	private String transactionId;
	


	public String getAttach() {
		return attach;
	}

	public void setAttach(String attach) {
		this.attach = attach;
	}

	public String getNonceStr() {
		return nonceStr;
	}

	public void setNonceStr(String nonceStr) {
		this.nonceStr = nonceStr;
	}

	public Double getNotifyBill() {
		return notifyBill;
	}

	public void setNotifyBill(Double notifyBill) {
		this.notifyBill = notifyBill;
	}

	public String getNotifyTimeEnd() {
		return notifyTimeEnd;
	}

	public void setNotifyTimeEnd(String notifyTimeEnd) {
		this.notifyTimeEnd = notifyTimeEnd;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public Double getOrderBill() {
		return orderBill;
	}

	public void setOrderBill(Double orderBill) {
		this.orderBill = orderBill;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getOutTradeNo() {
		return outTradeNo;
	}

	public void setOutTradeNo(String outTradeNo) {
		this.outTradeNo = outTradeNo;
	}

	public String getPayRemark() {
		return payRemark;
	}

	public void setPayRemark(String payRemark) {
		this.payRemark = payRemark;
	}

	public String getPaySign() {
		return paySign;
	}

	public void setPaySign(String paySign) {
		this.paySign = paySign;
	}

	public String getPayStatus() {
		return payStatus;
	}

	public void setPayStatus(String payStatus) {
		this.payStatus = payStatus;
	}

	public String getPkg() {
		return pkg;
	}

	public void setPkg(String pkg) {
		this.pkg = pkg;
	}

	public String getSignType() {
		return signType;
	}

	public void setSignType(String signType) {
		this.signType = signType;
	}

	public String getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

}
