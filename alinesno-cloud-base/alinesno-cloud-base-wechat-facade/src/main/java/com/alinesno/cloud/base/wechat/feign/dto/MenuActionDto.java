package com.alinesno.cloud.base.wechat.feign.dto;

import com.alinesno.cloud.common.facade.feign.BaseDto;

/**
 * <p> 传输对象</p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:11:54
 */
@SuppressWarnings("serial")
public class MenuActionDto extends BaseDto {

    /**
     * 消息创建时间 （整型）
     */
	private String createTime;
	
    /**
     * 事件类型，subscribe(订阅)、unsubscribe(取消订阅)
     */
	private String event;
	
    /**
     * 事件KEY值，设置的跳转URL
     */
	private String eventKey;
	
    /**
     * 发送方帐号（一个OpenID）
     */
	private String fromUserName;
	
    /**
     * 菜单id
     */
	private String menuId;
	
    /**
     * 消息类型，event
     */
	private String msgType;
	
    /**
     * 开发者微信号
     */
	private String toUserName;
	
	private String actionId;
	


	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public String getEventKey() {
		return eventKey;
	}

	public void setEventKey(String eventKey) {
		this.eventKey = eventKey;
	}

	public String getFromUserName() {
		return fromUserName;
	}

	public void setFromUserName(String fromUserName) {
		this.fromUserName = fromUserName;
	}

	public String getMenuId() {
		return menuId;
	}

	public void setMenuId(String menuId) {
		this.menuId = menuId;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public String getToUserName() {
		return toUserName;
	}

	public void setToUserName(String toUserName) {
		this.toUserName = toUserName;
	}

	public String getActionId() {
		return actionId;
	}

	public void setActionId(String actionId) {
		this.actionId = actionId;
	}

}
