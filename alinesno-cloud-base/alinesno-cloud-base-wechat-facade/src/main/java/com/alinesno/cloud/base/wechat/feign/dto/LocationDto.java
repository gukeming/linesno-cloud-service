package com.alinesno.cloud.base.wechat.feign.dto;

import com.alinesno.cloud.common.facade.feign.BaseDto;

/**
 * <p> 传输对象</p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:11:54
 */
@SuppressWarnings("serial")
public class LocationDto extends BaseDto {

    /**
     * 消息创建时间 （整型）
     */
	private String createTime;
	
    /**
     * 事件类型，subscribe(订阅)、unsubscribe(取消订阅)
     */
	private String event;
	
    /**
     * 发送方帐号（一个OpenID）
     */
	private String fromUserName;
	
    /**
     * 地理位置纬度
     */
	private String latitude;
	
    /**
     * 地理位置经度
     */
	private String longitude;
	
    /**
     * 消息类型，event
     */
	private String msgType;
	
    /**
     * 地理位置精度
     */
	private String precisions;
	
    /**
     * 开发者微信号
     */
	private String toUserName;
	
	private String locationId;
	


	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public String getFromUserName() {
		return fromUserName;
	}

	public void setFromUserName(String fromUserName) {
		this.fromUserName = fromUserName;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public String getPrecisions() {
		return precisions;
	}

	public void setPrecisions(String precisions) {
		this.precisions = precisions;
	}

	public String getToUserName() {
		return toUserName;
	}

	public void setToUserName(String toUserName) {
		this.toUserName = toUserName;
	}

	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

}
