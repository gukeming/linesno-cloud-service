package com.alinesno.cloud.base.wechat.feign.facade;

import org.springframework.cloud.openfeign.FeignClient;
import com.alinesno.cloud.common.facade.feign.IBaseFeign;
import com.alinesno.cloud.base.wechat.feign.dto.ErrorCodeDto;

/**
 * <p>  请求客户端 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:11:54
 */
@FeignClient(name="alinesno-cloud-base-wechat" , path="errorCode")
public interface ErrorCodeFeigin extends IBaseFeign<ErrorCodeDto> {

}
