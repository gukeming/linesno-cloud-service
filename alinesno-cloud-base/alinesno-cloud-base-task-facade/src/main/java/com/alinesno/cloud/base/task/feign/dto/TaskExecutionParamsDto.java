package com.alinesno.cloud.base.task.feign.dto;

import com.alinesno.cloud.common.facade.feign.BaseDto;

/**
 * <p> 传输对象</p>
 *
 * @author LuoAnDong
 * @since 2019-01-25 14:28:42
 */
@SuppressWarnings("serial")
public class TaskExecutionParamsDto extends BaseDto {

	private Long taskExecutionId;
	
	private String taskParam;
	


	public Long getTaskExecutionId() {
		return taskExecutionId;
	}

	public void setTaskExecutionId(Long taskExecutionId) {
		this.taskExecutionId = taskExecutionId;
	}

	public String getTaskParam() {
		return taskParam;
	}

	public void setTaskParam(String taskParam) {
		this.taskParam = taskParam;
	}

}
