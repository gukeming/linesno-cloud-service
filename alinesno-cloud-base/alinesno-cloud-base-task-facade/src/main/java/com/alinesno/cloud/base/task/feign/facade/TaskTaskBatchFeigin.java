package com.alinesno.cloud.base.task.feign.facade;

import org.springframework.cloud.openfeign.FeignClient;
import com.alinesno.cloud.common.facade.feign.IBaseFeign;
import com.alinesno.cloud.base.task.feign.dto.TaskTaskBatchDto;

/**
 * <p>  请求客户端 </p>
 *
 * @author LuoAnDong
 * @since 2019-01-25 14:28:42
 */
@FeignClient(name="alinesno-cloud-base-task" , path="taskTaskBatch")
public interface TaskTaskBatchFeigin extends IBaseFeign<TaskTaskBatchDto> {

}
