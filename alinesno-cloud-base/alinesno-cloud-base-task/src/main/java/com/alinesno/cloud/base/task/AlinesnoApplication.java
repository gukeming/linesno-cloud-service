package com.alinesno.cloud.base.task;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing ;
import com.ctrip.framework.apollo.spring.annotation.EnableApolloConfig;

/**
 * 启动入口<br/>
 * @EnableSwagger2 //开启swagger2 
 * 
 * @author LuoAnDong 
 * @since 2019-01-25 14:01:571
 */
@EnableJpaAuditing // jpa注解支持
@EnableApolloConfig //阿波罗分布式配置
@EnableAsync // 开启异步任务
@EnableEurekaClient  // 开启eureka
@SpringBootApplication
public class AlinesnoApplication {

	public static void main(String[] args) {
		SpringApplication.run(AlinesnoApplication.class, args);
	}

}
