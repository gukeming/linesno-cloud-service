package com.alinesno.cloud.common.facade.wrapper;

import java.io.Serializable;

import org.springframework.data.domain.Sort;

import com.alinesno.cloud.common.facade.pageable.RestPage;

/**
 * 条件基类
 * 
 * @author LuoAnDong
 * @since 2018年12月15日 上午8:32:20
 */
public abstract class Wrapper implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4005829114789471495L;
	private RestPage<?> pageable; // 分页
	private Sort sort; // 排序

	public RestPage<?> getPageable() {
		return pageable;
	}

	public void setPageable(RestPage<?> pageable) {
		this.pageable = pageable;
	}

	public Sort getSort() {
		return sort;
	}

	public void setSort(Sort sort) {
		this.sort = sort;
	}
	
	

}
