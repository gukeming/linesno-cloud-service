package com.alinesno.cloud.common.facade.feign;

import java.util.List;
import java.util.Optional;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.alinesno.cloud.common.facade.pageable.RestPage;
import com.alinesno.cloud.common.facade.wrapper.RestWrapper;

/**
 * 基础调用接口
 * 
 * @author LuoAnDong
 * @since 2018年12月2日 下午1:11:26
 * @param <DTO>
 */
public interface IBaseFeign<E> {

	@GetMapping("findAll")
	public List<E> findAll();

	@PostMapping("findAllById")
	public List<E> findAllById(@RequestBody Iterable<String> ids);

	@PostMapping("findAllByPageable")
	public RestPage<E> findAll(@RequestBody RestPage<E> restPage);

	@PostMapping("findAllByWrapper")
	public List<E> findAll(@RequestBody RestWrapper restWrapper);

	@PostMapping("findOneByWrapper")
	public Optional<E> findOne(@RequestBody RestWrapper restWrapper);

	@PostMapping("findAllByWrapperAndPageable")
	public RestPage<E> findAllByWrapperAndPageable(@RequestBody RestWrapper restWrapper);

	@PostMapping("saveAll")
	public List<E> saveAll(@RequestBody Iterable<E> entities);

	@PostMapping("saveAndFlush")
	public E saveAndFlush(@RequestBody E entity);

	@PostMapping("deleteInBatch")
	public void deleteInBatch(@RequestBody Iterable<E> entities);
	
	@PostMapping("deleteByIds")
	public void deleteByIds(@RequestBody String[] ids);

	@GetMapping("deleteAllInBatch")
	public void deleteAllInBatch();

	@GetMapping("getOne")
	public E getOne(@RequestParam("id") String id);

	@PostMapping("save")
	public E save(@RequestBody E entity);

	@GetMapping("findById")
	public Optional<E> findById(@RequestParam("id") String id);

	@GetMapping("existsById")
	public boolean existsById(@RequestParam("id") String id);

	@GetMapping("count")
	public long count();

	@GetMapping("deleteById")
	public void deleteById(@RequestParam("id") String id);

	@PostMapping("delete")
	public void delete(@RequestBody E entity);

	@PostMapping("deleteAllByIterable")
	public void deleteAll(@RequestBody Iterable<E> entities);

	@PostMapping("deleteAll")
	public void deleteAll();

	/**
	 * 更新实体状态 
	 * @param id
	 * @return
	 */
	@GetMapping("modifyHasStatus")
	boolean modifyHasStatus(@RequestParam("id") String id);

	@GetMapping("findAllByApplicationId")
	List<E> findAllByApplicationId(@RequestParam("applicationId") String applicationId);
	
	@GetMapping("findAllByTenantId")
	List<E> findAllByTenantId(@RequestParam("tenantId") String tenantId);
}
