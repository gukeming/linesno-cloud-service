package com.alinesno.cloud.common.facade.scan;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FeignClient;

import cn.hutool.core.util.ClassUtil;

/**
 *  Feign扫描工具类
 * @author LuoAnDong
 * @since 2019年4月7日 上午7:51:51
 */
public class FeignScanUtil {

	private static final Logger log = LoggerFactory.getLogger(FeignScanUtil.class) ; 

	/**
	 * 扫描feigin包路径 
	 * @param SelectClasser
	 * @return
	 */
	public static Set<Class<?>> scanFeigin(Class<?> SelectClasser){
		String scanBasePath = ClassUtil.getPackage(SelectClasser);
		String scanPath = scanBasePath.substring(0, scanBasePath.lastIndexOf(".enable")) ;
		
		System.out.println("scan path = " + scanPath);
		
		Set<Class<?>> sList = ClassUtil.scanPackageByAnnotation(scanPath, FeignClient.class) ; 
		if(log.isDebugEnabled()) {
			for(Class<?> s : sList) {
				log.debug("scan path:{} , class name:{}" , scanPath ,  s.getName());
			}	
		}
		
		return sList ; 
	}
	
}
