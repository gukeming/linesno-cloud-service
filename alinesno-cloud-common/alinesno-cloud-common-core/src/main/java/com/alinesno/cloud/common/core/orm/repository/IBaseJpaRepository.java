package com.alinesno.cloud.common.core.orm.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;

/**
 * 工作基础类
 * @author LuoAnDong
 * @since 2018年9月23日 下午5:57:51
 * @param <T>
 * @param <K>
 */
@NoRepositoryBean
public interface IBaseJpaRepository<T , K> extends JpaRepository<T, K>  , JpaSpecificationExecutor<T>{

	List<T> findAllByApplicationId(String applicationId);

	List<T> findAllByTenantId(String tenantId);

	List<T> findAllByTenantIdAndApplicationId(String tenantId, String applicationId);
	
	
}
