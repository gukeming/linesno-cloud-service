package com.alinesno.cloud.common.web.base.advice;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface TranslateCode {

	/**
	 * 类型
	 * @return
	 */
	public String types() default "" ; 

	/**
	 * 转换的字段
	 * @return
	 */
	public String value() default "" ; 

	/**
	 * 自定义插件 
	 * @return
	 */
	public String plugin() default "" ; 
	
	/**
	 * 应用的字段
	 * @return
	 */
	public String applicationField() default "" ; 
	
	/**
	 * 所属资源
	 * @return
	 */
	public String resourceField() default "";
}