package com.alinesno.cloud.common.web.enable;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.type.AnnotationMetadata;

import com.alinesno.cloud.common.core.auto.CoreImportProvider;
import com.alinesno.cloud.common.web.base.controller.PageJumpController;
import com.alinesno.cloud.common.web.base.controller.WebStorageController;
import com.alinesno.cloud.common.web.base.exceptions.GlobalExceptionHandler;
import com.alinesno.cloud.common.web.base.form.FormTokenInterceptor;
import com.alinesno.cloud.common.web.base.utils.WebUploadUtils;

/**
 * 引入自动类
 * @author LuoAnDong
 * @sine 2019年4月5日 下午3:34:07
 */
public class CommonWebConfigurationSelector implements ImportSelector {

	@Override
	public String[] selectImports(AnnotationMetadata importingClassMetadata) {
		List<String> importBean = new ArrayList<String>() ; 

		// common core 
		List<String> coreLoader = CoreImportProvider.classLoader() ; 
		importBean.addAll(coreLoader) ; 
	
		// common web 
		importBean.add(FormTokenInterceptor.class.getName()) ; 
		importBean.add(GlobalExceptionHandler.class.getName()) ; 
		importBean.add(WebUploadUtils.class.getName()) ; 
		
		importBean.add(WebStorageController.class.getName()) ; 
		importBean.add(PageJumpController.class.getName()) ; 
				
		return importBean.toArray(new String[] {}) ;
	}

	
}
