// 全局脚本
$(function(){  
	var layerIndex ; 
    $.ajaxSetup({
        type: "POST",
    	contentType : "application/json;charset=utf-8",
    	timeout: 30000 ,
	    dataType:'json',
    	beforeSend:function(xhr){
        	console.log("ajax beforeSend = "+xhr) ;
        	layerIndex = layer.load(1, {shade: [0.8,'#000']});
    	},success:function(result){
        	console.log("ajax success = "+result) ;
        	if(result.status != 200){
        		layer.alert(result.desc) ; 
        	}
    	},
    	complete : function(XMLHttpRequest, textStatus) {
        	console.log("ajax complete = "+XMLHttpRequest + " , status = " + textStatus) ;
        	if(layerIndex != null){
        		layer.close(layerIndex) ; 
        	}
        },
        error: function(jqXHR, textStatus, errorThrown) {
        	console.log("ajax error = " + jqXHR + " , status = " + textStatus) ; 
        	layer.alert("服务响应失败,请联系管理员.") ; 
        }
   });
});
