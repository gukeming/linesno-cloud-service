package com.alinesno.cloud.portal.desktop.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alinesno.cloud.common.core.constants.SpringInstanceScope;
import com.alinesno.cloud.common.web.base.controller.BaseController;

/**
 * 显示设置
 * @author LuoAnDong
 * @since 2019年4月19日 上午6:18:05
 */
@Controller
@Scope(SpringInstanceScope.PROTOTYPE)
@RequestMapping(value = "portal/deskweb/item/")
public class DisplayController extends BaseController {
	
	@RequestMapping(value = "display")
	public void display(Model model , HttpServletRequest request) {
	}
	
}
