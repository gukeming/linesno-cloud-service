package com.alinesno.cloud.base.boot.web.platform;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alinesno.cloud.base.boot.feign.dto.ManagerApplicationDto;
import com.alinesno.cloud.base.boot.feign.facade.ManagerApplicationFeigin;
import com.alinesno.cloud.base.boot.web.BeanMethodController;

/**
 * 应用管理
 * 
 * @author LuoAnDong
 * @since 2018年12月7日 下午10:58:20
 */
@Controller
@RequestMapping("application")
public class ApplicationController extends BeanMethodController<ManagerApplicationDto , ManagerApplicationFeigin> {

	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(ApplicationController.class);

}
