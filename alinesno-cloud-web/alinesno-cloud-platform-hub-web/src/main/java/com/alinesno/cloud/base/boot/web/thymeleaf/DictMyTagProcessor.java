package com.alinesno.cloud.base.boot.web.thymeleaf;

import org.springframework.context.ApplicationContext;
import org.thymeleaf.context.ITemplateContext;
import org.thymeleaf.model.IModel;
import org.thymeleaf.model.IModelFactory;
import org.thymeleaf.model.IProcessableElementTag;
import org.thymeleaf.processor.element.AbstractElementTagProcessor;
import org.thymeleaf.processor.element.IElementTagStructureHandler;
import org.thymeleaf.spring5.context.SpringContextUtils;
import org.thymeleaf.templatemode.TemplateMode;

/**
 * @ClassName DictMyTagProcessor
 * @Description 按钮权限自定义标签：功能类
 * @Author asus
 * @Date Created by asus on 2018/11/2111:53
 * @Version 1.0
 **/
public class DictMyTagProcessor extends AbstractElementTagProcessor {

	private static final String TAG_NAME = "tag";// 标签名 tag 这个玩意就是 自定义标签的 ： tag， 应该是可以定义多个标签
	private static final int PRECEDENCE = 1000;// 优先级

	public DictMyTagProcessor(final String dialectPrefix) {
		super(TemplateMode.HTML, // 此处理器将仅应用于HTML模式
				dialectPrefix, // 要应用于名称的匹配前缀
				TAG_NAME, // 标签名称：匹配此名称的特定标签
				true, // 没有应用于标签名的前缀
				null, // 无属性名称：将通过标签名称匹配
				false, // 没有要应用于属性名称的前缀
				PRECEDENCE// 优先(内部方言自己的优先
		);
	}

	/**
	 * @Author asus
	 * @Descript context 页面上下文 tag 标签
	 * @Date 10:27 2018/11/23
	 * @Param [context, tag, structureHandler]
	 * @return void
	 **/
	@Override
	protected void doProcess(final ITemplateContext context, final IProcessableElementTag tag,
			final IElementTagStructureHandler structureHandler) {

		// 获取应用程序上下文。
		ApplicationContext appCtx = SpringContextUtils.getApplicationContext(context);
		
		final IModelFactory modelFactory = context.getModelFactory();
		final IModel model = modelFactory.createModel();
		
		StringBuffer sb = new StringBuffer() ; 
		sb.append("this is my tags") ; 
		
		model.add(modelFactory.createText(sb));
		
		structureHandler.replaceWith(model, false);
	}
}