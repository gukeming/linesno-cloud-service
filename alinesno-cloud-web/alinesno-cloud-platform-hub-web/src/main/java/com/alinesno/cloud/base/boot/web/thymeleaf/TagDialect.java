package com.alinesno.cloud.base.boot.web.thymeleaf;
import java.util.HashSet;
import java.util.Set;

import org.thymeleaf.dialect.AbstractProcessorDialect;
import org.thymeleaf.processor.IProcessor;
import org.thymeleaf.standard.processor.StandardXmlNsTagProcessor;
import org.thymeleaf.templatemode.TemplateMode;

/**
 * @ClassName CustomLabel
 * @Description 按钮权限自定义标签名:tags
 * @Author asus
 * @Date Created by asus on 2018/11/2112:02
 * @Version 1.0
 **/
public class TagDialect extends AbstractProcessorDialect{

    public TagDialect() {
        super("Tags Dialect", "tags", 1000);
    }
    /**
     *@Author asus
     *@Descript元素处理器：“matter”标签。
     *@Date 16:05 2018/11/21
     *@Param [s]
     *@return java.util.Set<org.thymeleaf.processor.IProcessor>
     **/
    @Override
    public Set<IProcessor> getProcessors(final String dialectPrefix) {
        final Set<IProcessor> processors = new HashSet<>();
        processors.add(new DictMyTagProcessor(dialectPrefix));//添加我们定义的标签
        processors.add(new StandardXmlNsTagProcessor(TemplateMode.HTML, dialectPrefix));
        return processors;
    }
}