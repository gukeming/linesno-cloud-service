package com.alinesno.cloud.base.boot.web.thymeleaf;

import java.util.HashSet;
import java.util.Set;

import org.thymeleaf.dialect.AbstractProcessorDialect;
import org.thymeleaf.processor.IProcessor;

public class CustomDialect extends AbstractProcessorDialect{
    
    private static final String DIALECT_NAME = "staticFile";
    private static final String PREFIX = "W";
    public static final int PROCESSOR_PRECEDENCE = 1000;
    
    // @Value("${im.static.resources}")
    private String filePath = "templates" ;
    

    public CustomDialect() {
        super(DIALECT_NAME, PREFIX, PROCESSOR_PRECEDENCE);
    }

    @Override
    public Set<IProcessor> getProcessors(String dialectPrefix) {
        final Set<IProcessor> processors = new HashSet<IProcessor>();
        processors.add(new SampleJsTagProcessor(dialectPrefix, filePath));
//        processors.add(new SampleCssTagProcessor(dialectPrefix, filePath));
//        processors.add(new SampleSrcTagProcessor(dialectPrefix, filePath));
        return processors;
    }

}