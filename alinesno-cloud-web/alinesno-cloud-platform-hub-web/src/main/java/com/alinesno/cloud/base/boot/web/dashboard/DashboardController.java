package com.alinesno.cloud.base.boot.web.dashboard;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alinesno.cloud.common.web.base.controller.BaseController;

/**
 * 控制层
 * @author LuoAnDong
 * @since 2018年11月27日 上午6:41:40
 */
@Controller
public class DashboardController extends BaseController {

	private static final Logger log = LoggerFactory.getLogger(DashboardController.class) ; 
	
	@RequestMapping("/dashboard")
    public String dashboard(){
		log.debug("dashboard");
		
		return "dashboard/dashboard" ; 
    }
	
	@RequestMapping("/home")
    public void home(){
		log.debug("home");
    }
	
}
