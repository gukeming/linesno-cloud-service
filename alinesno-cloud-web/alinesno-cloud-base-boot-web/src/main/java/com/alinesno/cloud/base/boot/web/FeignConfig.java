package com.alinesno.cloud.base.boot.web;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import feign.Logger;

/**
 * 日志配置
 * @author LuoAnDong
 * @since 2019年3月3日 下午6:21:26
 */
@Configuration
public class FeignConfig {

    @Bean
    Logger.Level feignLevel() {
        return Logger.Level.FULL;
    }
}