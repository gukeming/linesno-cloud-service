package com.alinesno.cloud.base.boot.web.module.organization;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alinesno.cloud.base.boot.feign.dto.ManagerAccountDto;
import com.alinesno.cloud.base.boot.feign.dto.ManagerRoleDto;
import com.alinesno.cloud.base.boot.feign.facade.ManagerAccountFeigin;
import com.alinesno.cloud.base.boot.feign.facade.ManagerRoleFeigin;
import com.alinesno.cloud.base.boot.web.module.BeanMethodController;
import com.alinesno.cloud.common.core.constants.SpringInstanceScope;
import com.alinesno.cloud.common.web.base.advice.TranslateCode;
import com.alinesno.cloud.common.web.base.bean.DatatablesPageBean;
import com.alinesno.cloud.common.web.base.form.FormToken;
import com.alinesno.cloud.common.web.base.response.ResponseBean;
import com.alinesno.cloud.common.web.base.response.ResponseGenerator;
import com.alinesno.cloud.common.web.login.aop.AccountRecord;

/**
 * 后台账户管理 
 * @author LuoAnDong
 * @since 2019年3月24日 下午1:16:44
 */
@Controller
@Scope(SpringInstanceScope.PROTOTYPE)
@RequestMapping("boot/platform/account")
public class AccountController extends BeanMethodController<ManagerAccountDto , ManagerAccountFeigin> {

	private static final Logger log = LoggerFactory.getLogger(AccountController.class) ; 

	@Autowired
	private ManagerAccountFeigin managerAccountFeigin ; 
	
	@Autowired
	private ManagerRoleFeigin managerRoleFeigin ; 
	
	@RequestMapping("/list")
    public void list(){
    }

	@TranslateCode(value="[{hasStatus:has_status,rolePower:role_power}]" , plugin="departmentTranslatePlugin")
	@ResponseBody
	@PostMapping("/datatables")
    public DatatablesPageBean datatables(HttpServletRequest request , Model model ,  DatatablesPageBean page){
		log.debug("page = {}" , ToStringBuilder.reflectionToString(page));
		return this.toPage(model, managerAccountFeigin , page) ;
    }

	/**
	 * 认证
	 */
	@GetMapping("/auth")
    public ResponseBean auth(String accountId){
		log.debug("id = {}" , accountId);
		Optional<ManagerAccountDto> bean = managerAccountFeigin.findById(accountId) ; 
		return ResponseGenerator.ok(bean.get()) ; 
    }

	/**
	 * 认证
	 */
	@ResponseBody
	@PostMapping("/role")
    public List<ManagerRoleDto> role(String accountId){
		log.debug("id = {}" , accountId);
		List<ManagerRoleDto> bean = managerRoleFeigin.findByAccountId(accountId) ; 
		return bean ; 
    }
	
	/**
	 * 认证
	 */
	@ResponseBody
	@PostMapping("/authAccount")
    public ResponseBean authAccount(String accountId , String rolesId){
		log.debug("id = {}" , accountId);
	
		Optional<ManagerAccountDto> bean = managerAccountFeigin.findById(accountId) ; 
		boolean isAuth = managerRoleFeigin.authAccount(bean.get(), rolesId) ; 
		
		return ResponseGenerator.ok(isAuth) ; 
    }

	/**
	 * 详情
	 */
	@ResponseBody
	@GetMapping("/detail")
    public ResponseBean detail(String id){
		log.debug("id = {}" , id);
		Optional<ManagerAccountDto> bean = managerAccountFeigin.findById(id) ; 
		return ResponseGenerator.ok(bean.get()) ; 
    }

	/**
	 * 保存
	 * @param model
	 * @param request
	 * @param managerApplicationDto
	 * @return
	 */
	@ResponseBody
	@PostMapping("/update")
	public ResponseBean update(Model model , HttpServletRequest request, ManagerAccountDto dto) {
		
		ManagerAccountDto oldBean = managerAccountFeigin.getOne(dto.getId()) ; 
		BeanUtils.copyProperties(dto, oldBean);
		
		dto = managerAccountFeigin.save(oldBean) ; 
		return ResponseGenerator.ok(null) ; 	
	}
	
	/**
	 * 删除
	 */
	@ResponseBody
	@PostMapping("/delete")
    public ResponseBean delete(@RequestParam(value = "rowsId[]") String[] rowsId){
		log.debug("rowsId = {}" , ToStringBuilder.reflectionToString(rowsId));
		if(rowsId != null && rowsId.length > 0) {
			managerAccountFeigin.deleteByIds(rowsId); 
		}
		return ResponseGenerator.ok(null) ; 
    }

	/**
	 * 菜单管理查询功能  
	 * @return
	 */
	@FormToken(save=true)
	@GetMapping("/add")
    public void add(Model model , HttpServletRequest request){
    }
	
	/**
	 * 保存新对象 
	 * @param model
	 * @param managerCodeDto
	 * @return
	 */
	@AccountRecord("保存新账户.")
	@FormToken(remove=true)
	@ResponseBody
	@PostMapping("/save")
	public ResponseBean save(Model model , HttpServletRequest request, ManagerAccountDto dto) {
		log.debug("account dto:{}" , ToStringBuilder.reflectionToString(dto));
		dto = managerAccountFeigin.save(dto) ; 
		return ResponseGenerator.ok(null) ; 	
	}

}
