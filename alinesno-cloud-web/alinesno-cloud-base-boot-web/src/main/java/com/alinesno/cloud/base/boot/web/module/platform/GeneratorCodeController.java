package com.alinesno.cloud.base.boot.web.module.platform;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alinesno.cloud.base.boot.feign.dto.ManagerCodeDto;
import com.alinesno.cloud.base.boot.feign.dto.ManagerCodeTypeDto;
import com.alinesno.cloud.base.boot.feign.facade.ManagerCodeFeigin;
import com.alinesno.cloud.base.boot.feign.facade.ManagerCodeTypeFeigin;
import com.alinesno.cloud.common.core.constants.SpringInstanceScope;
import com.alinesno.cloud.common.facade.pageable.RestPage;
import com.alinesno.cloud.common.facade.wrapper.RestWrapper;
import com.alinesno.cloud.common.web.base.advice.TranslateCode;
import com.alinesno.cloud.common.web.base.bean.DatatablesPageBean;
import com.alinesno.cloud.common.web.base.controller.BaseController;
import com.alinesno.cloud.common.web.base.form.FormToken;
import com.alinesno.cloud.common.web.base.response.ResponseBean;
import com.alinesno.cloud.common.web.base.response.ResponseGenerator;

/**
 * 字典控制层
 * @author LuoAnDong
 * @since 2018年11月27日 上午6:41:40
 */
@Controller
@RequestMapping("boot/platform/generatorCode")
@Scope(SpringInstanceScope.PROTOTYPE)
public class GeneratorCodeController extends BaseController {

	private static final Logger log = LoggerFactory.getLogger(GeneratorCodeController.class) ; 

	@Autowired
	private ManagerCodeFeigin managerCodeFeigin ; 
	
	@Autowired
	private ManagerCodeTypeFeigin managerCodeTypeFeigin ; 

	/**
	 * 菜单管理查询功能  
	 * @return
	 */
	@GetMapping("/list")
    public void list(){
    }

	/**
	 * 保存新对象 
	 * @param model
	 * @param managerCodeDto
	 * @return
	 */
	@FormToken(remove=true)
	@ResponseBody
	@PostMapping("/save")
	public ResponseBean save(Model model , HttpServletRequest request, ManagerCodeDto managerCodeDto) {
		managerCodeDto = managerCodeFeigin.save(managerCodeDto) ; 
		return ResponseGenerator.ok(null) ; 	
	}
	
	/**
	 * 菜单管理查询功能  
	 * @return
	 */
	@FormToken(save=true)
	@GetMapping("/add")
    public void add(Model model , HttpServletRequest request){
		List<ManagerCodeTypeDto> codeTypes = managerCodeTypeFeigin.findAll() ; 
		model.addAttribute("codeTypes", codeTypes) ; 
    }
	
	/**
	 * 菜单管理查询功能  
	 * @return
	 */
	@FormToken(save=true)
	@GetMapping("/modify")
    public void modify(Model model , String id){
		Assert.hasLength(id , "主键不能为空.");
		ManagerCodeDto code = managerCodeFeigin.getOne(id) ; 
		
		List<ManagerCodeTypeDto> codeTypes = managerCodeTypeFeigin.findAll() ; 
		model.addAttribute("codeTypes", codeTypes) ; 
		model.addAttribute("bean", code) ; 
    }
	
	/**
	 * 删除
	 */
	@ResponseBody
	@PostMapping("/delete")
    public ResponseBean delete(@RequestParam(value = "rowsId[]") String[] rowsId){
		log.debug("rowsId = {}" , ToStringBuilder.reflectionToString(rowsId));
		if(rowsId != null && rowsId.length > 0) {
			managerCodeFeigin.deleteByIds(rowsId); 
		}
		return ResponseGenerator.ok(null) ; 
    }

	@TranslateCode
	@ResponseBody
	@RequestMapping("/datatables")
    public DatatablesPageBean datatables(HttpServletRequest request , Model model ,  DatatablesPageBean page){
		log.debug("page = {}" , ToStringBuilder.reflectionToString(page));
		return this.toPage(model, managerCodeFeigin , page) ;
    }

	private DatatablesPageBean toPage(Model model, ManagerCodeFeigin managerCodeFeigin, DatatablesPageBean page) {
		
		RestWrapper restWrapper = new RestWrapper() ; 
		RestPage<ManagerCodeDto> pageable = new RestPage<ManagerCodeDto>(page.getStart() / page.getLength(), page.getLength()) ; 
		restWrapper.setPageable(pageable); 
		restWrapper.builderCondition(page.getCondition()) ; 
		
		RestPage<ManagerCodeDto> pageableResult = managerCodeFeigin.findAllByWrapperAndPageable(restWrapper) ; 
		
		DatatablesPageBean p = new DatatablesPageBean();
		p.setData(pageableResult.getContent());
		p.setDraw(page.getDraw());
		p.setRecordsFiltered((int) pageableResult.getTotalElements());
		p.setRecordsTotal((int) pageableResult.getTotalElements());
		
		return p ;
	}
	
}
















