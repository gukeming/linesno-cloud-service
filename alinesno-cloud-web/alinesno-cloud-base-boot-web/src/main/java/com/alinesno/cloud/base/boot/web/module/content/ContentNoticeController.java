package com.alinesno.cloud.base.boot.web.module.content;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alinesno.cloud.base.boot.feign.dto.ContentNoticeDto;
import com.alinesno.cloud.base.boot.feign.facade.ContentNoticeFeigin;
import com.alinesno.cloud.common.core.constants.SpringInstanceScope;
import com.alinesno.cloud.common.facade.pageable.RestPage;
import com.alinesno.cloud.common.facade.wrapper.RestWrapper;
import com.alinesno.cloud.common.web.base.advice.TranslateCode;
import com.alinesno.cloud.common.web.base.bean.DatatablesPageBean;
import com.alinesno.cloud.common.web.base.controller.BaseController;
import com.alinesno.cloud.common.web.base.form.FormToken;
import com.alinesno.cloud.common.web.base.response.ResponseBean;
import com.alinesno.cloud.common.web.base.response.ResponseGenerator;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;

/**
 * 消息通知
 * 
 * @author LuoAnDong
 * @since 2019年4月4日 下午2:09:22
 */
@Controller
@RequestMapping("boot/platform/notice")
@Scope(SpringInstanceScope.PROTOTYPE)
public class ContentNoticeController extends BaseController {

	private static final Logger log = LoggerFactory.getLogger(ContentNoticeController.class);

	@Autowired
	private ContentNoticeFeigin contentNoticeFeigin ; 
	
	/**
	 * 代码管理查询功能
	 * 
	 * @return
	 */
	@GetMapping("/list")
	public void list() {
	}

	/**
	 * 保存新对象
	 * 
	 * @param model
	 * @param managerCodeDto
	 * @return
	 */
	@FormToken(remove = true)
	@ResponseBody
	@PostMapping("/save")
	public ResponseBean save(Model model, HttpServletRequest request, ContentNoticeDto managerCodeDto) {
		managerCodeDto = contentNoticeFeigin.save(managerCodeDto);
		return ResponseGenerator.ok(null);
	}

	/**
	 * 代码管理查询功能
	 * 
	 * @return
	 */
	@FormToken(save = true)
	@GetMapping("/add")
	public void add(Model model, HttpServletRequest request) {
	}

	/**
	 * 代码管理查询功能
	 * 
	 * @return
	 */
	@FormToken(save = true)
	@GetMapping("/modify")
	public void modify(Model model, HttpServletRequest request ,  String id) {
		Assert.hasLength(id, "主键不能为空.");
		ContentNoticeDto code = contentNoticeFeigin.getOne(id);
		model.addAttribute("bean", code);
	}

	/**
	 * 删除
	 */
	@ResponseBody
	@PostMapping("/delete")
	public ResponseBean delete(@RequestParam(value = "rowsId[]") String[] rowsId) {
		log.debug("rowsId = {}", ToStringBuilder.reflectionToString(rowsId));
		if (rowsId != null && rowsId.length > 0) {
			contentNoticeFeigin.deleteByIds(rowsId);
		}
		return ResponseGenerator.ok(null);
	}

	@TranslateCode("[{hasStatus:has_status}]")
	@ResponseBody
	@RequestMapping("/datatables")
	public DatatablesPageBean datatables(HttpServletRequest request, Model model, DatatablesPageBean page) {
		log.debug("page = {}", ToStringBuilder.reflectionToString(page));
		return this.toPage(model, contentNoticeFeigin, page);
	}

	private DatatablesPageBean toPage(Model model, ContentNoticeFeigin contentNoticeFeigin, DatatablesPageBean page) {

		RestWrapper restWrapper = new RestWrapper();
		RestPage<ContentNoticeDto> pageable = new RestPage<ContentNoticeDto>(page.getStart() / page.getLength(),page.getLength());
		restWrapper.setPageable(pageable);
		restWrapper.builderCondition(page.getCondition());

		RestPage<ContentNoticeDto> pageableResult = contentNoticeFeigin.findAllByWrapperAndPageable(restWrapper);

		DatatablesPageBean p = new DatatablesPageBean();
		p.setData(pageableResult.getContent());
		p.setDraw(page.getDraw());
		p.setRecordsFiltered((int) pageableResult.getTotalElements());
		p.setRecordsTotal((int) pageableResult.getTotalElements());

		return p;
	}
	
	/**
	 * 保存
	 * @param model
	 * @param request
	 * @param managerApplicationDto
	 * @return
	 */
	@FormToken(remove=true)
	@ResponseBody
	@PostMapping("/update")
	public ResponseBean updateType(Model model , HttpServletRequest request, ContentNoticeDto contentNoticeDto) {
		
		ContentNoticeDto oldBean = contentNoticeFeigin.getOne(contentNoticeDto.getId()) ; 
		BeanUtil.copyProperties(contentNoticeDto, oldBean , CopyOptions.create().setIgnoreNullValue(true));
		
		contentNoticeDto = contentNoticeFeigin.save(oldBean) ; 
		return ResponseGenerator.ok(null) ; 	
	}

}
