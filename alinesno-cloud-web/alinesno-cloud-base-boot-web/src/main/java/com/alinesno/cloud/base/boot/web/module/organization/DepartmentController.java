package com.alinesno.cloud.base.boot.web.module.organization;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alinesno.cloud.base.boot.feign.dto.ManagerDepartmentDto;
import com.alinesno.cloud.base.boot.feign.facade.ManagerDepartmentFeigin;
import com.alinesno.cloud.base.boot.web.module.BeanMethodController;
import com.alinesno.cloud.common.core.constants.SpringInstanceScope;
import com.alinesno.cloud.common.facade.wrapper.RestWrapper;
import com.alinesno.cloud.common.web.base.advice.TranslateCode;
import com.alinesno.cloud.common.web.base.bean.DatatablesPageBean;
import com.alinesno.cloud.common.web.base.form.FormToken;
import com.alinesno.cloud.common.web.base.response.ResponseBean;
import com.alinesno.cloud.common.web.base.response.ResponseGenerator;

/**
 * 部门管理 
 * @author LuoAnDong
 * @since 2019年3月24日 下午1:16:44
 */
@Controller
@Scope(SpringInstanceScope.PROTOTYPE)
@RequestMapping("boot/platform/department")
public class DepartmentController extends BeanMethodController<ManagerDepartmentDto , ManagerDepartmentFeigin> {
	
	private static final Logger log = LoggerFactory.getLogger(DepartmentController.class);

	@Autowired
	private ManagerDepartmentFeigin managerDepartmentFeigin ; 
	
	/**
	 * 树查询
	 */
	@GetMapping("/select")
    public void select(){
		log.debug("进入树列表选择页面.");
    }
		
	/**
	 * 保存新对象 
	 * @param model
	 * @param managerCodeDto
	 * @return
	 */
	@FormToken(remove=true)
	@ResponseBody
	@PostMapping("/save")
	public ResponseBean save(Model model , HttpServletRequest request, ManagerDepartmentDto managerCodeDto) {
		managerCodeDto = managerDepartmentFeigin.save(managerCodeDto) ; 
		return ResponseGenerator.ok(null) ; 	
	}
	
	/**
	 * 代码管理查询功能  
	 * @return
	 */
	@FormToken(save=true)
	@GetMapping("/add")
    public void add(Model model , HttpServletRequest request){
    }
	
	/**
	 * 代码管理查询功能  
	 * @return
	 */
	@FormToken(save=true)
	@GetMapping("/modify")
    public void modify(Model model , String id){
		Assert.hasLength(id , "主键不能为空.");
		ManagerDepartmentDto code = managerDepartmentFeigin.getOne(id) ; 
		model.addAttribute("bean", code) ; 
    }
	
	/**
	 * 删除
	 */
	@ResponseBody
	@PostMapping("/delete")
    public ResponseBean delete(@RequestParam(value = "rowsId[]") String[] rowsId){
		log.debug("rowsId = {}" , ToStringBuilder.reflectionToString(rowsId));
		if(rowsId != null && rowsId.length > 0) {
			managerDepartmentFeigin.deleteByIds(rowsId); 
		}
		return ResponseGenerator.ok(null) ; 
    }

	@TranslateCode("[{hasStatus:has_status}]")
	@ResponseBody
	@RequestMapping("/datatables")
    public DatatablesPageBean datatables(HttpServletRequest request , Model model ,  DatatablesPageBean page){
		log.debug("page = {}" , ToStringBuilder.reflectionToString(page));
		return this.toPage(model, managerDepartmentFeigin , page) ;
    }

//	private DatatablesPageBean toPage(Model model, ManagerDepartmentFeigin managerDepartmentFeigin, DatatablesPageBean page) {
//		
//		RestWrapper restWrapper = new RestWrapper() ; 
//		RestPage<ManagerDepartmentDto> pageable = new RestPage<ManagerDepartmentDto>(page.getStart() / page.getLength(), page.getLength()) ; 
//		restWrapper.setPageable(pageable); 
//		restWrapper.builderCondition(page.getCondition()) ; 
//		
//		RestPage<ManagerDepartmentDto> pageableResult = managerDepartmentFeigin.findAllByWrapperAndPageable(restWrapper) ; 
//		
//		DatatablesPageBean p = new DatatablesPageBean();
//		p.setData(pageableResult.getContent());
//		p.setDraw(page.getDraw());
//		p.setRecordsFiltered((int) pageableResult.getTotalElements());
//		p.setRecordsTotal((int) pageableResult.getTotalElements());
//		
//		return p ;
//	}

	/**
	 * 显示所有菜单数据
	 * @param model
	 * @param id
	 * @return
	 */
	@ResponseBody
	@GetMapping("/departmentData")
    public List<ManagerDepartmentDto> menusData(Model model , String id){
		
		log.debug("application id = {}" , id);
		model.addAttribute("applicationId", id) ; 
		
		RestWrapper restWrapper = new RestWrapper() ; 
		List<ManagerDepartmentDto> list = managerDepartmentFeigin.findAllWithApplication(restWrapper)  ; 
		
		return list ; 
    }

	/**
	 * 显示所有父级菜单
	 * @param model
	 * @param id
	 */
	@GetMapping("/parents")
    public void menus(Model model , String id){
		log.debug("application id = {}" , id);
		model.addAttribute("applicationId", id) ; 
    }
	
}
