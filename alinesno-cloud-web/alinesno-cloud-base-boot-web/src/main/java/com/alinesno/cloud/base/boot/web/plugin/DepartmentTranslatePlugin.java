package com.alinesno.cloud.base.boot.web.plugin;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alinesno.cloud.base.boot.feign.dto.ManagerDepartmentDto;
import com.alinesno.cloud.base.boot.feign.facade.ManagerDepartmentFeigin;
import com.alinesno.cloud.common.web.base.advice.TranslateCode;
import com.alinesno.cloud.common.web.base.advice.TranslatePlugin;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * 部门内容转换
 * @author LuoAnDong
 * @since 2019年4月7日 下午10:04:58
 */
@Component
public class DepartmentTranslatePlugin implements TranslatePlugin {

	@Autowired
	private ManagerDepartmentFeigin managerDepartmentFeigin ; 

	private String DEPARTMENT = "department" ; 
	
	@Override
	public void translate(ObjectNode node, TranslateCode convertCode) {
		JsonNode department = node.get(DEPARTMENT) ; 
		
		String value = "" ; 
		if(!department.isNull()) {
			log.debug("type node = {}" , department.asText());
			
			Optional<ManagerDepartmentDto> dto = managerDepartmentFeigin.findById(department.asText()) ; 
			if(dto.isPresent()) {
				value = dto.get().getFullName() ;
			}
		}
		node.put(DEPARTMENT + LABEL_SUFFER, value) ; 
	}

}
